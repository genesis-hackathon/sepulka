﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Sepulka.Services;

namespace Sepulka.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly TicketSignerService _signer;

        /// <summary>
        /// конструктор
        /// </summary>
        public TestController(TicketSignerService signer)
        {
            _signer = signer;
        }

        [HttpGet(nameof(Test))]
        public ActionResult<string> Test()
        {
            // билет
            var ticket = new Ticket
            {
                Route = "№30А «Через горы к Черному морю, через приют Фишт»",
                FIO = "Грибков Роман Вадимович",
                Document = "1234 567890",
                Category = "Взрослый",
                DateBegin = DateTime.Today.AddDays(5),
                DateEnd = DateTime.Today.AddDays(8),
            };

            using (var image = _signer.Sign(ticket))
            {
                var ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;
                return new FileStreamResult(ms, "image/png");
            }
        }
    }
}
