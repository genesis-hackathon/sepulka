﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Genesis.EPG.Services;

using Sepulka.Services;

namespace Sepulka.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QRCodeController : ControllerBase
    {
        private readonly EntityService _dataService;
        private readonly TicketMaker _ticketMaker;

        /// <summary>
        /// конструктор
        /// </summary>
        public QRCodeController(EntityService dataService, TicketMaker ticketMaker)
        {
            _dataService = dataService;
            _ticketMaker = ticketMaker;
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var data = _dataService.GetEntityItem("Ticket", id);
            if (data != default)
            {
                var ticket = data.ToObject<Ticket>();
                var image = _ticketMaker.MakeTicket(ticket);
                var ms = new MemoryStream();
                image.Save(ms, ImageFormat.Png);
                ms.Position = 0;
                return new FileStreamResult(ms, "image/png");
            }

            return NotFound();
        }
    }
}
