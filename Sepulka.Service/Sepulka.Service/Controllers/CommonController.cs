﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Sepulka.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        [HttpGet(nameof(TestConnection))]
        public ActionResult<string> TestConnection()
        {
            return "OK";
        }
    }
}
