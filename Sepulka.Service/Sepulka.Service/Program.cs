﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Sepulka.Service
{
    public class Program
    {
        /// <summary>
        /// конфигурация приложения
        /// </summary>
        public static IConfiguration Configuration { get; internal set; }

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
#if DEBUG
                .UseIISIntegration()
#else
                .UseKestrel()
                .UseUrls("http://0.0.0.0:5005/")
#endif
                .UseStartup<Startup>();
        }
    }
}
