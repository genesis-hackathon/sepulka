﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Genesis.Seraphim
{
    /// <summary>
    /// помощник для подготовки изображений
    /// </summary>
    public static class ImageHelper
    {
        /// <summary>
        /// получить изображение и превью из файла
        /// </summary>
        /// <param name="filename"> имя файла </param>
        /// <returns></returns>
        public static (string image, string thumbnail) GetImageAndThumbnail(string filename)
        {
            try
            {
                var image = Image.FromFile(filename);
                var base64 = GetBase64ImageString(image);
                return (base64, base64);
            }
            catch
            {
                return default;
            }
        }

        /// <summary>
        /// получить изображение и превью из изображения
        /// </summary>
        /// <param name="image"> изображение </param>
        /// <returns></returns>
        public static (string image, string thumbnail) GetImageAndThumbnail(Image image)
        {
            try
            {
                var base64 = GetBase64ImageString(image);
                return (base64, base64);
            }
            catch
            {
                return default;
            }
        }


        //public static Image GetReducedImage(int width, int height, Stream resourceImage)
        //{
        //    try
        //    {
        //        Image image = Image.FromStream(resourceImage);
        //        Image thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero);

        //        return thumb;
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        /// <summary>
        /// получить base64-представление изображения
        /// </summary>
        /// <param name="image"> изображение </param>
        /// <param name="format"> формат </param>
        /// <returns></returns>
        public static string GetBase64ImageString(Image image, ImageFormat format = default)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, format ?? ImageFormat.Jpeg);
                ms.Position = 0;
                var data = ms.ToArray();
                return Convert.ToBase64String(data);
            }
        }
    }
}
