﻿using System;

namespace Genesis.Seraphim
{
    /// <summary>
    /// форматы изображения
    /// </summary>
    public static class ImageFormats
    {
        /// <summary>
        /// JPEG
        /// </summary>
        public const string Jpg = "jpg";

        /// <summary>
        /// PNG
        /// </summary>
        public const string Png = "png";
    }
}
