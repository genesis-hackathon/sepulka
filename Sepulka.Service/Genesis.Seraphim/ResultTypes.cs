﻿using System;

namespace Genesis.Seraphim
{
    /// <summary>
    /// тип результата
    /// </summary>
    public enum ResultTypes
    {
        /// <summary>
        /// Запрос выполнен успешно
        /// </summary>
        Ok = 0,

        /// <summary>
        /// Не переданы все обязательные поля
        /// </summary>
        JsonFieldMissing = 100,

        /// <summary>
        /// Некорректный пакет
        /// </summary>
        ParsePacketError = 101,

        /// <summary>
        /// Ошибка парсинга. Не хватает закрывающей скобки или запятой
        /// </summary>
        JsonSyntaxError = 102,

        /// <summary>
        /// Сервис отправки сообщений не может обработать сообщение в данный момент
        /// </summary>
        SenderNotReady = 103,

        /// <summary>
        /// Файл с контентом не существует. Возможно не было передано изображение
        /// </summary>
        FileNotExist = 104,

        /// <summary>
        /// Некорректный получатель сообщения
        /// </summary>
        BadReceiver = 105,

        /// <summary>
        /// Сервер не дождался всех фрагментов команды
        /// </summary>
        Timeout = 106,

        /// <summary>
        /// Не совпал token аутентификации
        /// </summary>
        Unauthorized = 107,
    }
}
