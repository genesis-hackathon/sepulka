﻿using System;

using Newtonsoft.Json;

namespace Genesis.Seraphim
{
    /// <summary>
    /// запрос для подписки
    /// </summary>
    public class SubscribeApiMessage : ApiMessage
    {
        /// <summary>
        /// подписка на информацию
        /// </summary>
        [JsonProperty("subscribe")]
        public bool Subscribe { get; set; } = true;
    }
}
