﻿using System;

using Newtonsoft.Json;

namespace Genesis.Seraphim
{
    /// <summary>
    /// графическое сообщение
    /// </summary>
    public class ImageMessage : ApiMessage
    {
        /// <summary>
        /// изображение
        /// </summary>
        [JsonProperty("image", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Image { get; set; }

        /// <summary>
        /// превью
        /// </summary>
        [JsonProperty("imagethumbnail", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Thumbnail { get; set; }

        /// <summary>
        /// формат изображения
        /// </summary>
        [JsonProperty("imageformat", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ImageFormat { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="receiver"> получатель </param>
        /// <param name="image"> изображение </param>
        /// <param name="thumbnail"> превью </param>
        /// <param name="imageFormat"> формат изображения </param>
        public ImageMessage(string receiver, string image, string thumbnail, string imageFormat)
        {
            Receiver = receiver;
            Image = image;
            Thumbnail = thumbnail;
            ImageFormat = imageFormat;

            MimeType = MimeTypes.Image;
            ReceiverEncoding = "hash";
        }
    }
}
