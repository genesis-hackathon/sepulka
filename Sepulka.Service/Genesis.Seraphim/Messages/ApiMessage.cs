﻿using System;

using Newtonsoft.Json;

namespace Genesis.Seraphim
{
    /// <summary>
    /// базовый класс запроса
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("[{Opaque}] {Result}")]
    public class ApiMessage
    {
        /// <summary>
        /// аутентификация
        /// </summary>
        [JsonProperty("auth")]
        public string Auth { get; set; }

        /// <summary>
        /// идентификатор запроса
        /// </summary>
        [JsonProperty("opaque")]
        public int Opaque { get; set; }

        /// <summary>
        /// тип сообщения
        /// </summary>
        [JsonProperty("mimetype", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string MimeType { get; set; }

        /// <summary>
        /// текст сообщения
        /// </summary>
        [JsonProperty("text", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Text { get; set; }

        /// <summary>
        /// отправитель
        /// </summary>
        [JsonProperty("sender", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Sender { get; set; }

        /// <summary>
        /// получатель
        /// </summary>
        [JsonProperty("receiver", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Receiver { get; set; }

        /// <summary>
        /// кодирование получателя сообщения
        /// </summary>
        [JsonProperty("receiverencoding", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ReceiverEncoding { get; set; }

        /// <summary>
        /// тип результата
        /// </summary>
        [JsonProperty("result", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ResultTypes Result { get; set; }
    }
}
