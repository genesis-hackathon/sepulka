﻿using System;

using Newtonsoft.Json;

namespace Genesis.Seraphim
{
    /// <summary>
    /// текстовое сообщение
    /// </summary>
    public class TextMessage : ApiMessage
    {
        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="receiver"> получатель </param>
        /// <param name="message"> текст сообщения </param>
        public TextMessage(string receiver, string text)
        {
            Receiver = receiver;
            Text = text;

            MimeType = MimeTypes.Text;
            ReceiverEncoding = "hash";
        }
    }
}
