﻿using System;

namespace Genesis.Seraphim
{
    /// <summary>
    /// делегат события Серафима
    /// </summary>
    /// <param name="sender"> отправитель </param>
    /// <param name="e"> аргументы </param>
    public delegate void SeraphimEventHandler(object sender, SeraphimEventArgs e);

    /// <summary>
    /// аргументы
    /// </summary>
    public class SeraphimEventArgs : EventArgs
    {
        /// <summary>
        /// сообщение
        /// </summary>
        public ApiMessage Message { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public SeraphimEventArgs() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="message"> сообщение </param>
        public SeraphimEventArgs(ApiMessage message)
        {
            Message = message;
        }
    }
}
