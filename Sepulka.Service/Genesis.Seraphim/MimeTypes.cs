﻿using System;

namespace Genesis.Seraphim
{
    /// <summary>
    /// типы MIME
    /// </summary>
    public static class MimeTypes
    {
        /// <summary>
        /// Текстовое сообщение
        /// </summary>
        public const string Text = "com.seraphim.textmessage";

        /// <summary>
        /// Изображение
        /// </summary>
        public const string Image = "com.seraphim.imagemessage";
    }
}
