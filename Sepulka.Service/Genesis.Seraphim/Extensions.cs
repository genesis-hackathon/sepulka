﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Genesis.Seraphim
{
    /// <summary>
    /// расширения
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// конвертировать массив в список объектов
        /// </summary>
        /// <typeparam name="T"> тип объекта </typeparam>
        /// <param name="data"> данные </param>
        /// <returns></returns>
        public static List<T> ToObjectArray<T>(this JArray data)
        {
            if (data == default) return new List<T>();
            else return data.Select(e => e.ToObject<T>()).ToList();
        }
    }
}
