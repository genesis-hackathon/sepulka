﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Genesis.Seraphim
{
    /// <summary>
    /// коннектор серафима
    /// </summary>
    public class SeraphimConnector : IDisposable
    {
        /// <summary>
        /// токен авторизации
        /// </summary>
        public string AuthToken { get; set; } = "2126fd8cf637647229c544c86bd655a0df8f77dbc6c025ca8886c5221be3097a";
        //public string AuthToken { get; set; } = "1b190253fc80765a897f0d39b13d1774dd34ee77de2116e62bc7121ab6fb625b";

        /// <summary>
        /// адрес 
        /// </summary>
        //public string Address { get; set; } = "api.seraphim.online";
        public string Address { get; set; } = "31.184.209.253";

        /// <summary>
        /// порт
        /// </summary>
        public int Port { get; set; } = 20008;

        /// <summary>
        /// логин
        /// </summary>
        public string Login { get; set; } = "zapovednik8";

        private const int BUFFER_SIZE = 4096;   // размер буфера
        private TcpClient client;               // клиент
        private NetworkStream networkStream;    // поток
        private StreamReader reader;            // читатель
        private int opaque;                     // идентификатор запроса

        /// <summary>
        /// событие получения сообщения
        /// </summary>
        public event SeraphimEventHandler OnReveiveMessage;

        /// <summary>
        /// уничтожить объект
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (networkStream != default) networkStream.Dispose();
                if (client != default) client.Dispose();
                if (reader != default) reader.Dispose();
            }
            catch { }
        }

        /// <summary>
        /// подключиться
        /// </summary>
        public void Connect()
        {
            Dispose();

            client = new TcpClient(AddressFamily.InterNetwork);
            client.Connect(Address, Port);
            client.SendBufferSize = BUFFER_SIZE;
            client.ReceiveBufferSize = BUFFER_SIZE;
            networkStream = client.GetStream();
            opaque = 0;
            reader = new StreamReader(networkStream, Encoding.UTF8);
        }

        /// <summary>
        /// начать работу
        /// </summary>
        public void Begin()
        {
            SendMessage(new SubscribeApiMessage());

            Task.Run(async () =>
            {
                while (true)
                {
                    await Task.Delay(10000);
                    SendMessage(new ApiMessage());
                }
            });

            while (true)
            {
                var line = reader.ReadLine();
                if (line != default)
                {
                    var message = JsonConvert.DeserializeObject<ApiMessage>(line);
                    OnReveiveMessage?.Invoke(this, new SeraphimEventArgs(message));
                }
            }
        }

        /// <summary>
        /// отправить запрос
        /// </summary>
        /// <param name="request"> запрос </param>
        public void SendMessage(ApiMessage request)
        {
            // получаем JSON сообщения
            var json = JsonConvert.SerializeObject(SignRequest(request));

            // формируем и отправляем массив байт
            var data = Encoding.UTF8.GetBytes(json);
            for (int offset = 0, n = data.Length; offset < n; offset += BUFFER_SIZE)
            {
                networkStream.Write(data, offset, Math.Min(n - offset, BUFFER_SIZE));
            }
            networkStream.Flush();
        }

        /// <summary>
        /// подписать запрос
        /// </summary>
        /// <param name="request"> запрос </param>
        /// <returns></returns>
        private ApiMessage SignRequest(ApiMessage request)
        {
            request.Auth = AuthToken;
            request.Opaque = opaque++;
            return request;
        }

        /// <summary>
        /// отправить текстовое сообщение
        /// </summary>
        /// <param name="receiver"> получатель </param>
        /// <param name="message"> текст сообщения </param>
        public void SendTextMessage(string receiver, string message)
        {
            SendMessage(new TextMessage(receiver, message));
        }

        /// <summary>
        /// отправить графическое сообщение
        /// </summary>
        /// <param name="receiver"> получатель </param>
        /// <param name="image"> изображение </param>
        /// <param name="thumbnail"> превью </param>
        /// <param name="imageFormat"> формат изображения </param>
        public void SendImageMessage(string receiver, string image, string thumbnail, string imageFormat)
        {
            SendMessage(new ImageMessage(receiver, image, thumbnail, imageFormat));
        }
    }
}
