﻿using System;
using System.Text.RegularExpressions;

using Newtonsoft.Json;

namespace Sepulka
{
    /// <summary>
    /// билет
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// информация о маршруте
        /// </summary>
        [JsonProperty("r")]
        public string Route { get; set; }

        /// <summary>
        /// данные документа
        /// </summary>
        [JsonProperty("p")]
        public string Document { get; set; }

        /// <summary>
        /// категория билета
        /// </summary>
        [JsonProperty("c")]
        public string Category { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [JsonProperty("f")]
        public string FIO { get; set; }

        /// <summary>
        /// начальная дата
        /// </summary>
        [JsonProperty("b")]
        [JsonConverter(typeof(SimpleDateTimeConverter))]
        public DateTime DateBegin { get; set; }

        /// <summary>
        /// конечная дата
        /// </summary>
        [JsonProperty("e")]
        [JsonConverter(typeof(SimpleDateTimeConverter))]
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// указывает, что документ является паспортом
        /// </summary>
        [JsonIgnore]
        public bool IsPassport
        {
            get
            {
                return Regex.IsMatch(Document ?? "", @"^\d\d\d\d\s");
            }
        }

        /// <summary>
        /// представление интервала
        /// </summary>
        [JsonIgnore]
        public string IntervalDisplay
        {
            get
            {
                return $"{DateBegin.ToString("dd.MM")} – {DateEnd.ToString("dd.MM")}";
            }
        }
    }
}
