﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.Extensions.Configuration;

namespace Sepulka.Services
{
    /// <summary>
    /// генератор билетов
    /// </summary>
    public class TicketMaker
    {
        private const int SIZE = 50;

        /// <summary>
        /// сервис подписывания билетов
        /// </summary>
        private readonly TicketSignerService _signer;

        /// <summary>
        /// конструктор
        /// </summary>
        public TicketMaker(IConfiguration config)
        {
            _signer = new TicketSignerService(config);
        }

        /// <summary>
        /// сформировать изображение билета
        /// </summary>
        /// <param name="ticket"> билет </param>
        /// <returns></returns>
        public Image MakeTicket(Ticket ticket)
        {
            if (ticket == default) throw new ArgumentNullException(nameof(ticket));

            // получаем QR-код
            var qrCode = _signer.Sign(ticket);

            int w = SIZE * 8, h = SIZE * 12;

            var qrOffset = SIZE / 3;

            var bmp = new Bitmap(w, h);

            // шрифты
            var family = new FontFamily("Times");
            var font20 = new Font(family, 14);
            var font16 = new Font(family, 12);
            var font12 = new Font(family, 11);

            var lightBrush = new SolidBrush(Color.FromArgb(103, 103, 103));
            var darkBrush = new SolidBrush(Color.FromArgb(30, 30, 30));

            using (var g = Graphics.FromImage(bmp))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;

                g.Clear(Color.White);

                // рисуем подложку
                //g.DrawImage(LightenImage(back, 0.5), new Rectangle(0, 0, w, h), new Rectangle(0, 0, back.Width, back.Height), GraphicsUnit.Pixel);
                g.DrawImage(_background, new Rectangle(0, 0, w, h), new Rectangle(0, 0, _background.Width, _background.Height), GraphicsUnit.Pixel);

                // рисуем границы QR-кода
                g.FillRectangle(new SolidBrush(Color.FromArgb(128, 255, 255, 255)), new Rectangle(75 - qrOffset, 305 - qrOffset, 250 + qrOffset * 2, 250 + qrOffset * 2));

                // рисуем QR-код
                g.DrawImage(qrCode, new Rectangle(75, 305, 250, 250), new Rectangle(0, 0, qrCode.Width, qrCode.Height), GraphicsUnit.Pixel);

                // адрес
                g.DrawString("г.Сочи, Адлерский р-он,", font12, lightBrush, new Point(174, 30));
                g.DrawString("ул.Карла Маркса, 8", font12, lightBrush, new Point(174, 44));

                // добавляем надписи
                g.DrawString("Посетитель", font12, lightBrush, new Point(30, 100));
                g.DrawString(ticket.FIO, font20, darkBrush, new Point(30, 116));

                g.DrawString("Маршрут", font12, lightBrush, new Point(30, 159));
                {
                    var tokens = ticket.Route.Split(' ');
                    string val = "";
                    int pos = 175;
                    foreach (var item in tokens)
                    {
                        if (val.Length < 20)
                        {
                            val = val + " " + item;
                        }
                        else
                        {
                            g.DrawString(val.Trim(), font16, darkBrush, new Point(30, pos));
                            val = item;
                            pos += 19;
                        }
                    }
                    if (val != "")
                    {
                        g.DrawString(val.Trim(), font16, darkBrush, new Point(30, pos));
                    }
                }

                var culture = CultureInfo.GetCultureInfo("ru-RU") ?? CultureInfo.InvariantCulture;
                g.DrawString("Срок действия билета", font12, lightBrush, new Point(30, 233));
                g.DrawString($"{ticket.DateBegin.ToString("dd MMMM yyyy", culture)}г. – {ticket.DateEnd.ToString("dd MMMM yyyy", culture)}г.", font16, darkBrush, new Point(30, 249));
            }

            font20.Dispose();
            font16.Dispose();
            font12.Dispose();

            lightBrush.Dispose();

            return bmp;
        }

        //private Bitmap LightenImage(Bitmap bmp, double lightenAmount)
        //{
        //    var clone = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), PixelFormat.Format32bppArgb);

        //    for (int y = 0; y < clone.Height; y++)
        //    {
        //        for (int x = 0; x < clone.Width; x++)
        //        {
        //            var oldColor = clone.GetPixel(x, y);
        //            var newColor = LightenColor(oldColor, lightenAmount);
        //            clone.SetPixel(x, y, newColor);
        //        }
        //    }
        //    return clone;
        //}

        private Color LightenColor(Color color, double amount)
        {
            return Color.FromArgb(color.A, (int)Math.Min(255, color.R + 255 * amount), (int)Math.Min(255, color.G + 255 * amount), (int)Math.Min(255, color.B + 255 * amount));
        }

        private static readonly Bitmap _background;

        /// <summary>
        /// статический конструктор
        /// </summary>
        static TicketMaker()
        {
            const string resoureName = "Sepulka.back.png";
            _background = Image.FromStream(typeof(TicketMaker).Assembly.GetManifestResourceStream(resoureName)) as Bitmap;
        }
    }
}
