﻿using System;
using System.Security.Cryptography;

namespace Sepulka.Services
{
    /// <summary>
    /// опции сервиса подписывания
    /// </summary>
    public class TicketSignerOptions
    {
        /// <summary>
        /// размер ключа
        /// </summary>
        public int KeySize { get; set; }

        /// <summary>
        /// приватный ключ
        /// </summary>
        public string Parameters { get; set; }
    }
}
