﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Genesis.QRCodeLib;

namespace Sepulka.Services
{
    /// <summary>
    /// сервис подписывания билетов
    /// </summary>
    public class TicketSignerService
    {
        private readonly TicketSignerOptions _options;

        /// <summary>
        /// конструктор
        /// </summary>
        public TicketSignerService(IConfiguration config)
        {
            _options = config.GetSection("Signer").Get<TicketSignerOptions>();
        }

        /// <summary>
        /// подписать билет
        /// </summary>
        /// <param name="ticket"> билет </param>
        public Bitmap Sign(Ticket ticket)
        {
            // получаем JSON билета
            var json = JsonConvert.SerializeObject(ticket);

            byte[] data, sign;

            // используем RSA
            using (var rsa = new RSACryptoServiceProvider(_options.KeySize))
            {
                rsa.ImportParameters(JsonConvert.DeserializeObject<RSAParameters>(_options.Parameters));

                data = Encoding.UTF8.GetBytes(json);
                sign = rsa.SignData(data, new SHA1CryptoServiceProvider());
            }

            // формируем данные для QR-кода
            var qrCodeData = Encoding.UTF8.GetBytes($"{json}|{Convert.ToBase64String(sign)}");

            // формируем QR-код
            var encoder = new QREncoder();
            var qr = encoder.Encode(ErrorCorrection.L, qrCodeData);
            var image = GenerateImage(qr);

            //image.Save(@"C:\Temp\qrcode.png", System.Drawing.Imaging.ImageFormat.Png);

            return image;
        }

        /// <summary>
        /// сгенерировать изображение
        /// </summary>
        /// <param name="data"> изображение </param>
        /// <param name="pixelSize"> размер пикселя </param>
        /// <returns></returns>
        private Bitmap GenerateImage(bool[,] data, int pixelSize = 4)
        {
            int w = data.GetLength(1);
            int h = data.GetLength(0);

            int dw = w * pixelSize, dh = h * pixelSize;

            var bmp = new Bitmap(dw, dh);
            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                {
                    var color = data[x, y] ? Color.Black : Color.White;
                    for (int i = 0; i < pixelSize; i++)
                        for (int j = 0; j < pixelSize; j++)
                        {
                            bmp.SetPixel(x * pixelSize + i, y * pixelSize + j, color);
                        }
                }
            return bmp;
        }
    }
}
