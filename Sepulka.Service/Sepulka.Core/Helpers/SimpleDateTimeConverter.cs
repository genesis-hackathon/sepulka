﻿using Newtonsoft.Json.Converters;

namespace Sepulka
{
    /// <summary>
    /// простой конвертер дат
    /// </summary>
    public class SimpleDateTimeConverter : IsoDateTimeConverter
    {
        /// <summary>
        /// конструктор
        /// </summary>
        public SimpleDateTimeConverter()
        {
            DateTimeFormat = "yyyyMMdd";
        }
    }
}
