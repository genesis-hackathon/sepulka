﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Extensions.Configuration;

using Sepulka.Services;

namespace Sepulka
{
    class Program
    {
        /// <summary>
        /// конфигурация системы
        /// </summary>
        public static IConfiguration Configuration { get; private set; }

        static void Main()
        {
            Configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .Build();

            var ticket = new Ticket
            {
                Route = "Урочище Медвежьи ворота – Лагерь Холодный",
                FIO = "Грибков Роман Вадимович",
                Document = "1234 567890",
                DateBegin = DateTime.Today.AddDays(5),
                DateEnd = DateTime.Today.AddDays(8),
            };

            var maker = new TicketMaker(Program.Configuration);
            var image = maker.MakeTicket(ticket);
            image.Save(@"C:\Temp\ticket.png", ImageFormat.Png);
        }
    }
}
