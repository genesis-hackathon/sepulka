﻿using System;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// правило
    /// </summary>
    public interface IAction
    {
        /// <summary>
        /// выполнить действие
        /// </summary>
        /// <param name="bot"> бот </param>
        /// <param name="userState"> состояние </param>
        /// <param name="rule"> правило </param>
        void Do(SemaBot bot, UserState userState, IRule rule);
    }
}