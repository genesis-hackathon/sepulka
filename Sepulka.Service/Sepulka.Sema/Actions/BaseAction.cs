﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// базовое действие
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{GetType().Name,nq}")]
    public abstract class BaseAction : IAction
    {
        /// <summary>
        /// бот
        /// </summary>
        protected SemaBot bot;

        /// <summary>
        /// состояние пользователя
        /// </summary>
        protected UserState userState;

        /// <summary>
        /// правило
        /// </summary>
        protected IRule rule;

        /// <summary>
        /// данные билета
        /// </summary>
        protected TicketData Ticket => userState.TicketData;

        /// <summary>
        /// выполнить действие
        /// </summary>
        /// <param name="bot"> бот </param>
        /// <param name="userState"> состояние пользователя </param>
        /// <param name="rule"> правило </param>
        public virtual void Do(SemaBot bot, UserState userState, IRule rule)
        {
            this.bot = bot;
            this.userState = userState;
            this.rule = rule;

            OnDo();
        }

        /// <summary>
        /// выполнить действие
        /// </summary>
        protected virtual void OnDo() { }

        /// <summary>
        /// отправить сообщение
        /// </summary>
        /// <param name="message"> сообщение </param>
        protected virtual void SendMessage(string message)
        {
            bot.SendMessage(userState, message);
        }

        /// <summary>
        /// отправить картинку
        /// </summary>
        /// <param name="filename"> имя файла </param>
        /// <param name="imageFormat"> формат изображения </param>
        protected virtual void SendImage(string filename, string imageFormat)
        {
            bot.SendImage(userState, filename, imageFormat);
        }

        /// <summary>
        /// отправить картинку
        /// </summary>
        /// <param name="image"> изображение </param>
        /// <param name="imageFormat"> формат изображения </param>
        protected virtual void SendImage(Image image, string imageFormat)
        {
            bot.SendImage(userState, image, imageFormat);
        }

        /// <summary>
        /// сохранить список вариантов
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="list"> список </param>
        protected virtual void SaveList<T>(IEnumerable<T> list)
            where T : IDictionaryItem
        {
            userState.LastVariants = list.Select((e, i) => new DictionaryOption(e, i + 1)).ToList();
        }

        /// <summary>
        /// отформатировать список вариантов
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="list"> список </param>
        /// <returns></returns>
        protected string FormatList<T>(IEnumerable<T> list)
            where T : IDictionaryItem
        {
            return string.Join(Environment.NewLine, list.Select((e, i) => $"{i + 1} {e.Name}").ToArray());
        }

        /// <summary>
        /// сохранить список вариантов и отформатировать его
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="list"> список </param>
        /// <returns></returns>
        protected string SaveAndFormatList<T>(IEnumerable<T> list)
            where T : IDictionaryItem
        {
            SaveList(list);
            return FormatList(list);
        }

        /// <summary>
        /// выбираем последний вариант
        /// </summary>
        /// <returns></returns>
        protected DictionaryItem GetLastVariantDictionaryItem()
        {
            if (userState.LastVariants != default)
            {
                var selected = userState.LastVariants.Find(e => e.Option == userState.SelectedVariant);
                if (selected != default)
                {
                    return new DictionaryItem(selected);
                }
            }
            return default;
        }
    }
}
