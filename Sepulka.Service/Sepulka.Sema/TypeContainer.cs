﻿using System;
using System.Collections.Generic;
using System.Linq;

using Sepulka.Sema.Metadata;

namespace Sepulka.Sema
{
    /// <summary>
    /// контейнер типов
    /// </summary>
    public static class TypeContainer
    {
        private static readonly Dictionary<string, IAction> _actions;
        private static readonly Dictionary<string, IRule> _rules;
        private static readonly Dictionary<string, IState> _states;
        private static readonly Dictionary<string, Type> _stateTypes;

        /// <summary>
        /// статический конструктор
        /// </summary>
        static TypeContainer()
        {
            _actions = new Dictionary<string, IAction>(StringComparer.InvariantCulture);
            _rules = new Dictionary<string, IRule>(StringComparer.InvariantCulture);
            _states = new Dictionary<string, IState>(StringComparer.InvariantCulture);
            _stateTypes = new Dictionary<string, Type>(StringComparer.InvariantCulture);

            foreach (var type in from e in typeof(TypeContainer).Assembly.GetTypes()
                                 where e.IsClass &&
                                       !e.IsAbstract
                                 select e)
            {
                if (typeof(IAction).IsAssignableFrom(type))
                {
                    _actions[GetName(type.Name, "Action")] = Activator.CreateInstance(type) as IAction;
                }
                if (typeof(IRule).IsAssignableFrom(type))
                {
                    _rules[GetName(type.Name, "Rule")] = Activator.CreateInstance(type) as IRule;
                }
                if (typeof(IState).IsAssignableFrom(type))
                {
                    _stateTypes[GetName(type.Name, "State")] = type;
                }
            }
        }

        /// <summary>
        /// получить действие по имени
        /// </summary>
        /// <param name="name"> имя </param>
        /// <returns></returns>
        public static IAction GetAction(string name)
        {
            return _actions.TryGetValue(name, out var value) ? value : default;
        }

        /// <summary>
        /// получить правило по имени
        /// </summary>
        /// <param name="name"> имя </param>
        /// <returns></returns>
        public static IRule GetRule(string name)
        {
            return _rules.TryGetValue(name, out var value) ? value : default;
        }

        /// <summary>
        /// получить состояние по имени
        /// </summary>
        /// <param name="name"> имя </param>
        /// <returns></returns>
        public static IState GetState(string name)
        {
            if (!_stateTypes.TryGetValue(name, out var type))
            {
                return default;
            }
            if (!_states.TryGetValue(name, out var state))
            {
                // первоначальная инициализация состояния
                state = Activator.CreateInstance(type) as IState;
                state.Init();
                _states[name] = state;
            }
            return state;
        }

        /// <summary>
        /// получить имя компонента
        /// </summary>
        /// <param name="name"> имя типа </param>
        /// <param name="suffix"> суффикс </param>
        /// <returns></returns>
        private static string GetName(string name, string suffix)
        {
            if (name == default) throw new ArgumentNullException(nameof(name));

            if (name.EndsWith(suffix))
            {
                // удаляем суффикс
                name = name.Substring(0, name.Length - suffix.Length);
            }

            return name.Trim('_');
        }
    }
}
