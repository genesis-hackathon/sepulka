﻿using System;

namespace Sepulka.Sema
{
    public static class WordForm
    {
        /// <summary>
        /// выбрать корректную форму слова для числительного
        /// </summary>
        /// <param name="count"> число </param>
        /// <param name="form0"> форма 0 (ноль окон) </param>
        /// <param name="form1"> форма 1 (одно окно) </param>
        /// <param name="form2"> форма 2 (два окна) </param>
        /// <returns></returns>
        public static string Get(int count, string form0, string form1, string form2)
        {
            if ((count / 10) % 10 == 1)
            {
                return form0;
            }
            else
            {
                switch (count % 10)
                {
                    case 1:
                        return form1;
                    case 2:
                    case 3:
                    case 4:
                        return form2;
                    default:
                        return form0;
                }
            }
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного
        /// </summary>
        /// <param name="count"> число </param>
        /// <param name="forms"> массив форм </param>
        /// <returns></returns>
        public static string Get(int count, string[] forms)
        {
            if (forms == null || !(forms.Length == 1 || forms.Length == 3))
            {
                throw new ArgumentException("Передано неверное число форм слова", "forms");
            }
            else if (forms.Length == 1)
            {
                return Get(count, forms[0], forms[0], forms[0]);
            }
            else
            {
                return Get(count, forms[0], forms[1], forms[2]);
            }
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного и запись в форматированном виде
        /// </summary>
        /// <param name="format"> строка формата </param>
        /// <param name="count"> число </param>
        /// <param name="form0"> форма 0 (ноль окон) </param>
        /// <param name="form1"> форма 1 (одно окно) </param>
        /// <param name="form2"> форма 2 (два окна) </param>
        /// <returns></returns>
        public static string GetFormatted(string format, int count, string form0, string form1, string form2)
        {
            return string.Format(format, count, Get(count, form0, form1, form2));
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного и запись в форматированном виде
        /// </summary>
        /// <param name="format"> строка формата </param>
        /// <param name="count"> число </param>
        /// <param name="forms"> массив форм </param>
        /// <returns></returns>
        public static string GetFormatted(string format, int count, string[] forms)
        {
            return string.Format(format, count, Get(count, forms));
        }

        #region Long Forms

        /// <summary>
        /// выбрать корректную форму слова для числительного
        /// </summary>
        /// <param name="count"> число </param>
        /// <param name="form0"> форма 0 (ноль окон) </param>
        /// <param name="form1"> форма 1 (одно окно) </param>
        /// <param name="form2"> форма 2 (два окна) </param>
        /// <returns></returns>
        public static string Get(long count, string form0, string form1, string form2)
        {
            if ((count / 10) % 10 == 1)
            {
                return form0;
            }
            else
            {
                switch (count % 10)
                {
                    case 1:
                        return form1;
                    case 2:
                    case 3:
                    case 4:
                        return form2;
                    default:
                        return form0;
                }
            }
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного
        /// </summary>
        /// <param name="count"> число </param>
        /// <param name="forms"> массив форм </param>
        /// <returns></returns>
        public static string Get(long count, string[] forms)
        {
            if (forms == null || !(forms.Length == 1 || forms.Length == 3))
            {
                throw new ArgumentException("Передано неверное число форм слова", "forms");
            }
            else if (forms.Length == 1)
            {
                return Get(count, forms[0], forms[0], forms[0]);
            }
            else
            {
                return Get(count, forms[0], forms[1], forms[2]);
            }
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного и запись в форматированном виде
        /// </summary>
        /// <param name="format"> строка формата </param>
        /// <param name="count"> число </param>
        /// <param name="form0"> форма 0 (ноль окон) </param>
        /// <param name="form1"> форма 1 (одно окно) </param>
        /// <param name="form2"> форма 2 (два окна) </param>
        /// <returns></returns>
        public static string GetFormatted(string format, long count, string form0, string form1, string form2)
        {
            return string.Format(format, count, Get(count, form0, form1, form2));
        }

        /// <summary>
        /// выбрать корректную форму слова для числительного и запись в форматированном виде
        /// </summary>
        /// <param name="format"> строка формата </param>
        /// <param name="count"> число </param>
        /// <param name="forms"> массив форм </param>
        /// <returns></returns>
        public static string GetFormatted(string format, long count, string[] forms)
        {
            return string.Format(format, count, Get(count, forms));
        }

        #endregion
    }
}
