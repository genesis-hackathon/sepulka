﻿/*

MIT License

Copyright (c) 2019 Копытов Дмитрий

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

using System;

namespace Genesis.CV.NumberUtils
{
    /// <summary>
    /// настройки парсера
    /// </summary>
    public class RussianNumberParserOptions
    {
        /// <summary>
        /// предельное значение ошибки в токене, чтобы его можно было использовать для распознавания
        /// </summary>
        public double MaxTokenError { get; set; } = 0.67;

        /// <summary>
        /// величина, на которую ухудшается результат после деления подстроки на более мелкие части
        /// </summary>
        public double SplitErrorValue { get; set; } = 0.1;

        /// <summary>
        /// настройки по умолчанию
        /// </summary>
        private static RussianNumberParserOptions _default;

        /// <summary>
        /// настройки по умолчанию
        /// </summary>
        public static RussianNumberParserOptions Default
        {
            get
            {
                return _default ?? (_default = new RussianNumberParserOptions());
            }
        }
    }
}
