﻿using Newtonsoft.Json;

namespace Sepulka.Sema
{
    /// <summary>
    /// справочник
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("[{ID}] {Name,nq}")]
    public class DictionaryItem : IDictionaryItem
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ID { get; set; }

        /// <summary>
        /// наименование
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public DictionaryItem() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="id"> идентификатор </param>
        /// <param name="name"> наименование </param>
        public DictionaryItem(int id, string name)
        {
            ID = id;
            Name = name;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="origin"> образец </param>
        public DictionaryItem(IDictionaryItem origin)
        {
            ID = origin.ID;
            Name = origin.Name;
        }
    }
}
