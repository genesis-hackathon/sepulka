﻿using System;
using Microsoft.Extensions.Configuration;

using Genesis.EPG;

namespace Sepulka.Sema
{
    public class Program
    {
        /// <summary>
        /// конфигурация системы
        /// </summary>
        public static IConfiguration Configuration { get; private set; }

        /// <summary>
        /// конфигурация EPG
        /// </summary>
        public static EPGConfig EPGConfig { get; private set; }

        static void Main()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            EPGConfig = Configuration.GetSection(nameof(EPGConfig)).Get<EPGConfig>();
            EPGConfig.ConnectionString = Configuration.GetConnectionString(EPGConfig.ConnectionStringName ?? "DB");

            using (var bot = new SemaBot())
            {
                bot.Init();
                bot.Run();
            }
        }
    }
}
