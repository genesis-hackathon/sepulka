﻿using Newtonsoft.Json;

namespace Sepulka.Sema
{
    /// <summary>
    /// элемент справочника с позицией
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("#{Option} [{ID}] {Name,nq}")]
    public class DictionaryOption : DictionaryItem
    {
        /// <summary>
        /// номер позиции
        /// </summary>
        [JsonProperty("option")]
        public int Option { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public DictionaryOption() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="origin"> образец </param>
        /// <param name="option"> номер позиции </param>
        public DictionaryOption(IDictionaryItem origin, int option) : base(origin)
        {
            Option = option;
        }
    }
}
