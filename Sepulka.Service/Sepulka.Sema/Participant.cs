﻿using System;

using Newtonsoft.Json;

namespace Sepulka.Sema
{
    /// <summary>
    /// данные участника
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{FIO,nq}, {Document,nq}, {Category,nq}")]
    public class Participant
    {
        /// <summary>
        /// ФИО
        /// </summary>
        [JsonProperty("fio", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string FIO { get; set; }

        /// <summary>
        /// данные документа
        /// </summary>
        [JsonProperty("document", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Document { get; set; }

        /// <summary>
        /// категория билета (идентификатор)
        /// </summary>
        [JsonProperty("category_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int CategoryID { get; set; }

        /// <summary>
        /// категория билета (наименование
        /// </summary>
        [JsonProperty("category_name", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string CategoryName { get; set; }

        /// <summary>
        /// копировать в другой элемент
        /// </summary>
        /// <param name="target"> целевой элемент </param>
        public void CopyTo(Participant target)
        {
            target.CategoryID = CategoryID;
            target.CategoryName = CategoryName;
            target.Document = Document;
            target.FIO = FIO;
        }
    }
}
