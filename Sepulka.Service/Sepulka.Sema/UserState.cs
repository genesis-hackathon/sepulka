﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Sepulka.Sema
{
    /// <summary>
    /// состояние пользователя
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("[{ID}] {UserID}")]
    public class UserState
    {
        /// <summary>
        /// идентификатор пользователя вбазе
        /// </summary>
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ID { get; set; }

        /// <summary>
        /// идентификатор пользователя в Серафиме
        /// </summary>
        [JsonProperty("seraphim_id")]
        public string UserID { get; set; }

        /// <summary>
        /// имя состояния
        /// </summary>
        [JsonProperty("state_name", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string StateName { get; set; }

        /// <summary>
        /// последний сохраненный ввод пользователя
        /// </summary>
        [JsonProperty("last_input", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LastInput { get; set; }

        /// <summary>
        /// последний список вариантов
        /// </summary>
        [JsonProperty("last_variants", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<DictionaryOption> LastVariants { get; set; } = new List<DictionaryOption>();

        /// <summary>
        /// выбранный вариант
        /// </summary>
        [JsonProperty("selected_variant", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int SelectedVariant { get; set; }

        /// <summary>
        /// данные билета
        /// </summary>
        [JsonProperty("ticket_data", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public TicketData TicketData { get; set; } = new TicketData();

        /// <summary>
        /// данные состояния
        /// </summary>
        [JsonProperty("state_data", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Dictionary<string, string> StateData { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// очистить пользовательские данные
        /// </summary>
        public void Clear()
        {
            StateData.Clear();
            TicketData = new TicketData();
            LastInput = default;
            LastVariants.Clear();
            SelectedVariant = default;
        }
    }
}
