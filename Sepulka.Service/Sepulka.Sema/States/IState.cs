﻿using System;
using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// состояние
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// список триггеров
        /// </summary>
        List<Trigger> Triggers { get; set; }

        /// <summary>
        /// инициализровать состояние
        /// </summary>
        void Init();
    }
}
