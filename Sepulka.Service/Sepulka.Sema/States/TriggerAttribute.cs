﻿using System;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// атрибут триггера
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    [System.Diagnostics.DebuggerDisplay("{RuleType,nq}, {ActionType,nq} => {NextState,nq}")]
    public class TriggerAttribute : Attribute
    {
        /// <summary>
        /// тип правила
        /// </summary>
        public string RuleType { get; set; }

        /// <summary>
        /// тип действия
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        /// следующее состояние
        /// </summary>
        public string NextState { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="ruleType"> тип правила </param>
        /// <param name="actionType"> тип действия </param>
        /// <param name="nextState"> следующее состояние </param>
        public TriggerAttribute(string ruleType, string actionType, string nextState)
        {
            RuleType = ruleType;
            ActionType = actionType;
            NextState = nextState;
        }
    }
}