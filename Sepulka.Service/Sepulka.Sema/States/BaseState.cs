﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// базовое состояние
    /// </summary>
    [Trigger("Hello", "HelloFirst", "Ready")]
    [System.Diagnostics.DebuggerDisplay("{GetType().Name,nq}")]
    public abstract class BaseState : IState
    {
        /// <summary>
        /// список триггеров
        /// </summary>
        public List<Trigger> Triggers { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public BaseState()
        {
            Triggers = new List<Trigger>();
        }

        /// <summary>
        /// инициализровать состояние
        /// </summary>
        public virtual void Init()
        {
            // автоматическое наполнение
            foreach (var item in from e in GetType().GetCustomAttributes(typeof(TriggerAttribute), true)
                                 select e as TriggerAttribute)
            {
                var rule = TypeContainer.GetRule(item.RuleType);
                var action = TypeContainer.GetAction(item.ActionType);
                Triggers.Add(new Trigger(rule, action, item.NextState));
            }

            OnInit();
        }

        /// <summary>
        /// инициализровать состояние
        /// </summary>
        protected virtual void OnInit() { }
    }
}
