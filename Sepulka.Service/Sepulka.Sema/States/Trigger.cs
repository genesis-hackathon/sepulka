﻿using System;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// триггер
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{Rule,q}, {Action,nq} => {NextState}")]
    public class Trigger
    {
        /// <summary>
        /// правило
        /// </summary>
        public IRule Rule { get; set; }

        /// <summary>
        /// действие
        /// </summary>
        public IAction Action { get; set; }

        /// <summary>
        /// следующее состояние
        /// </summary>
        public string NextState { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public Trigger() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="rule"> правило </param>
        /// <param name="action"> действие </param>
        /// <param name="nextState"> следующее состояние </param>
        public Trigger(IRule rule, IAction action, string nextState)
        {
            Rule = rule;
            Action = action;
            NextState = nextState;
        }
    }
}
