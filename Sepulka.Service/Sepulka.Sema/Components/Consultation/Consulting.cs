﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Sepulka.Sema.Data;

namespace Sepulka.Sema.Metadata
{
    [Trigger("ValidDuration", "DurationSelected", "DurationSelected")]
    [Trigger("TooLowDuration", "TooLowDuration", "ReservationConsulting")]
    [Trigger("TooHighDuration", "TooHighDuration", "ReservationConsulting")]
    [Trigger("Any", "InvalidDuration", "ReservationConsulting")]
    public class ReservationConsultingState : BaseState
    {
    }

    public class ReservationConsultingAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage(@"Я помогу Вам подобрать маршрут. Сколько дней Вы хотите провести в походе?");
        }
    }

    [Trigger("ValidDuration", "DurationSelected", "DurationSelected")]
    [Trigger("TooLowDuration", "TooLowDuration", "ServiceConsulting")]
    [Trigger("TooHighDuration", "TooHighDuration", "ServiceConsulting")]
    [Trigger("Any", "InvalidDuration", "ServiceConsulting")]
    public class ServiceConsultingState : BaseState
    {
    }

    public class ServiceConsultingAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage(@"На сколько дней планируете поход?");
        }
    }

    public abstract class BaseDurationRule : BaseRule
    {
        protected int? ParseDuration()
        {
            if (int.TryParse(text, out var value))
            {
                // выбрано число дней
                return value;
            }
            else
            {
                var res = Genesis.CV.NumberUtils.RussianNumber.Parse(text);
                if (res.Error < 0.25)
                {
                    value = (int)res.Value;
                    return value;
                }
            }
            return default;
        }
    }

    public class ValidDurationRule : BaseDurationRule
    {
        protected override bool OnCheck()
        {
            var value = ParseDuration();
            if (value.HasValue && value.Value > 0 && value.Value <= 100)
            {
                userState.StateData["Duration"] = value.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class TooLowDuration : BaseDurationRule
    {
        protected override bool OnCheck()
        {
            var value = ParseDuration();
            return value.HasValue && value.Value < 0;
        }
    }

    public class TooHighDuration : BaseDurationRule
    {
        protected override bool OnCheck()
        {
            var value = ParseDuration();
            return value.HasValue && value.Value > 100;
        }
    }

    public class TooLowDurationAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Сёма не умеет смотреть в прошлое, выберите, пожалуйста, корректную продолжительность похода.");
        }
    }

    public class TooHighDurationAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Таких долгих походов не бывает. Увы :({Environment.NewLine}Выберите продолжительность поменьше.");
        }
    }

    public class InvalidDurationAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Боюсь, моя твоя не понимать. Попробуй еще раз.");
        }
    }

    [Trigger("SelectVariant", "SelectTopService", "ServiceSelected")]
    public class DurationSelectedState : BaseState
    {
    }

    public class DurationSelectedAction : BaseAction
    {
        protected override void OnDo()
        {
            var sb = new StringBuilder();

            var duration = int.Parse(userState.StateData["Duration"]);
            sb.AppendLine($"Вы ищете маршрут продолжительностью в {duration} {WordForm.Get(duration, "дней", "день", "дня")}.");

            // ищем маршруты
            List<Service> services;
            if (Ticket.Reservation != default)
            {
                // выбран заповедник
                services = bot.Data.Services.Where(e => e.ReservationID == Ticket.Reservation.ID).ToList();
            }
            else
            {
                // ничего не выбрано
                services = bot.Data.Services.ToList();
            }

            // фильтруем
            var filtered = services.Where(e => duration >= e.MinDuration && duration <= e.MaxDuration).ToList();
            if (filtered.Count == 0)
            {
                // не удалось найти подходящие маршруты
                sb.AppendLine("К сожалению, маршрутов указанной вами продолжительности нет, по, быть может, вам подойдут эти варианты?");
                var top = services.OrderBy(e => Math.Abs(e.AverageDuration - duration)).Take(SmartText.DEFAULT_TOP);
                sb.AppendLine(SaveAndFormatList(top));
            }
            else
            {
                sb.AppendLine("Мы подобрали для Вас маршруты, выберите один из них:");
                var top = filtered.OrderBy(e => Math.Abs(e.AverageDuration - duration)).Take(SmartText.DEFAULT_TOP);
                sb.AppendLine(SaveAndFormatList(top));
            }

            SendMessage(sb.ToString().TrimEnd());
        }
    }
}
