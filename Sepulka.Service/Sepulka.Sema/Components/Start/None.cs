﻿namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// начальное состояние
    /// </summary>
    [Trigger("Any", "HelloFirst", "Ready")]
    public class None : BaseState
    {
    }
}
