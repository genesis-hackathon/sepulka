﻿using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    public class HelloRule : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
#if DEBUG
            "0",
#endif
            "привет",
            "стоп",
            "в начало",
            "сначала",
            "давай сначала",

            // english
            "hello",
            "stop",
        };

        public override bool Check(SemaBot bot, UserState state, string text)
        {
            return _variants.Contains(text.ToLowerInvariant());
        }
    }

    public class HelloFirstAction : BaseAction
    {
        protected override void OnDo()
        {
            // начинаем с чистого листа
            userState.Clear();

            SendMessage(@"Добро пожаловать в Сочи!
Я помогу с выбором маршрута и покупкой билета на маршруты Сочинского национального парка и Кавказского природного биосферного заповедника. Вы уже определились куда хотите пойти?");
        }
    }
}
