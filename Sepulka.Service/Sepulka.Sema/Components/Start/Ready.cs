﻿namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// начальное состояние
    /// </summary>
    [Trigger("Yes", "WaitForReservation", "WaitForReservation")]
    [Trigger("No", "ReservationConsulting", "ReservationConsulting")]
    [Trigger("FullReservationName", "ReservationSelected", "ReservationSelected")]
    [Trigger("PartialReservationName", "ReservationAgree", "ReservationAgree")]
    [Trigger("AnySave", "ShowTopReservationsActionAfterBadInput", "ShowTopReservations")]
    public class Ready : BaseState
    {
    }
}
