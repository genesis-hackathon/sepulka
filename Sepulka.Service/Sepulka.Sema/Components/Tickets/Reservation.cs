﻿using System;

namespace Sepulka.Sema.Metadata
{
    [Trigger("FullReservationName", "ReservationSelected", "ReservationSelected")]
    [Trigger("PartialReservationName", "ReservationAgree", "ReservationAgree")]
    [Trigger("AnySave", "ShowTopReservationsActionAfterBadInput", "ShowTopReservations")]
    public class WaitForReservationState : BaseState
    {
    }

    public class WaitForReservationAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage(@"Здорово! Напишите мне куда вы хотите пойти.");
        }
    }

    public class ReservationSelectedAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Вы выбрали «{Ticket.Reservation.Name}».");
            SendMessage("Уже определились с маршрутом или помочь?");
        }
    }

    /// <summary>
    /// полное совпадение
    /// </summary>
    public class FullReservationName : BaseRule
    {
        protected override bool OnCheck()
        {
            SaveLastInput();
            var result = SmartText.CheckFullDictionaryName(bot.Data.Reservations, text);
            if (result.Found)
            {
                // сохраняем заповедник
                userState.TicketData.Reservation = new DictionaryItem(result.Item);
            }
            return result.Found;
        }
    }

    /// <summary>
    /// частичное совпадение
    /// </summary>
    public class PartialReservationName : BaseRule
    {
        protected override bool OnCheck()
        {
            SaveLastInput();
            var result = SmartText.CheckPartialDictionaryName(bot.Data.Reservations, text);
            if (result.Found)
            {
                // сохраняем заповедник
                userState.TicketData.Reservation = new DictionaryItem(result.Item);
            }
            return result.Found;
        }
    }

    public class ReservationAgreeAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Вы имели в виду «{Ticket.Reservation.Name}»?");
        }
    }

    [Trigger("Yes", "ReservationSelected", "ReservationSelected")]
    [Trigger("No", "ShowTopReservations", "ShowTopReservations")]
    public class ReservationAgreeState : BaseState
    {
    }

    /// <summary>
    /// показать топ похожих
    /// </summary>
    public class ShowTopReservationsAction : BaseAction
    {
        protected override void OnDo()
        {
            var top = SmartText.SelectTopByName(bot.Data.Reservations, userState.LastInput);
            var message = $"Предлагаю вам выбрать один из вариантов:{Environment.NewLine}{SaveAndFormatList(top)}";
            SendMessage(message);
        }
    }

    public class ShowTopReservationsActionAfterBadInput : BaseAction
    {
        protected override void OnDo()
        {
            var top = SmartText.SelectTopByName(bot.Data.Reservations, userState.LastInput, SmartTextModes.ByWord);
            var message = $"Не удалось найти подходящий вариант :({Environment.NewLine}Предлагаю вам выбрать один из списка:{Environment.NewLine}{SaveAndFormatList(top)}";
            SendMessage(message);
        }
    }

    [Trigger("SelectVariant", "SelectTopReservation", "ReservationSelected")]
    [Trigger("Any", "Empty", "ShowTopReservations")]
    public class ShowTopReservationsState : BaseState
    {
    }

    public class SelectTopReservationAction : BaseAction
    {
        protected override void OnDo()
        {
            // сохраняем заповедник
            userState.TicketData.Reservation = GetLastVariantDictionaryItem();
            SendMessage($"Вы выбрали «{Ticket.Reservation.Name}».");
            SendMessage("Уже определились с маршрутом или помочь?");
        }
    }
}
