﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Sepulka.Sema.Metadata
{
    [Trigger("AddParticipant", "AddParticipant", "AddParticipant")]
    [Trigger("Any", "EnterParticipant", "PeriodSelected")]
    public class PeriodSelectedState : BaseState
    {
    }

    public class PeriodSelectedAction : BaseAction
    {
        protected override void OnDo()
        {
            if (Ticket.IsOneDay)
            {
                SendMessage($"Вами выбран однодневый поход на дату {Ticket.DateBegin.ToString("dd.MM")}.");
            }
            else
            {
                SendMessage($"Вами выбран поход на период {Ticket.DateBegin.ToString("dd.MM")} – {Ticket.DateEnd.ToString("dd.MM")} ({Ticket.DayCount} {WordForm.Get(Ticket.DayCount, "дней", "день", "дня")}).");
            }
            SendMessage($"Отлично. Теперь давайте определимся с участниками.{Environment.NewLine}Один участник – одно сообщение в формате{Environment.NewLine}ФИО, серия и номер паспорта, возраст (полных лет).");
        }
    }

    [Trigger("Yes", "CheckParticipants", "CheckParticipants")]
    [Trigger("AddParticipant", "AddParticipant", "AddParticipant")]
    [Trigger("Any", "EnterParticipant", "AddParticipant")]
    public class AddParticipantState : BaseState
    {
    }

    public class AddParticipantRule : BaseRule
    {
        private static readonly Regex _regParticipant = new Regex(@"\s*([\w\s]+)\s*,\s*([\w\d\s\-\–]+)\s*,\s*(\d+)", RegexOptions.Compiled);

        protected override bool OnCheck()
        {
            var m = _regParticipant.Match(text);
            if (m.Success)
            {
                var fio = m.Groups[1].Value.Trim();
                var document = m.Groups[2].Value.Trim();
                var age = int.Parse(m.Groups[3].Value.Trim());

                if (fio.Length < 5) return false;
                if (document.Length < 10) return false;

                var participant = new Participant
                {
                    FIO = fio,
                    Document = document,
                    CategoryID = age < 7 ? 1 : (age < 15 ? 2 : 3),
                    CategoryName = age < 7 ? "0 – 6 лет" : (age < 15 ? "7 – 14 лет" : "Взрослый")
                };
                Ticket.AddParticipant(participant);

                return true;
            }

            return false;
        }
    }

    public class AddParticipantAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage("Это всё?");
        }
    }

    public class EnterParticipantAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Введите участника в формате{Environment.NewLine}ФИО, серия и номер паспорта, возраст (полных лет).");
        }
    }

    [Trigger("Yes", "Payment", "Ready")]
    //[Trigger("Yes", "Payment", "Payment")]
    [Trigger("No", "Pechalka", "None")]
    public class CheckParticipantsState : BaseState
    {
    }

    public class CheckParticipantsAction : BaseAction
    {
        protected override void OnDo()
        {
            var payments = bot.Data.Payments;
            var serviceID = Ticket.Service.ID;
            var days = Ticket.DayCount;

            var sb = new StringBuilder();
            sb.AppendLine("Ваш заказ:");
            sb.AppendLine("════════════════");
            if (Ticket.IsOneDay)
            {
                sb.AppendLine($"Однодневый поход на дату {Ticket.DateBegin.ToString("dd.MM")}.");
            }
            else
            {
                sb.AppendLine($"Поход на период {Ticket.DateBegin.ToString("dd.MM")} – {Ticket.DateEnd.ToString("dd.MM")} ({Ticket.DayCount} {WordForm.Get(Ticket.DayCount, "дней", "день", "дня")}).");
            }
            sb.AppendLine("════════════════");

            // считаем стоимость
            var cost = 0;
            foreach (var item in Ticket.Participants)
            {
                var pay = payments.Find(e => e.ServiceID == serviceID && e.CategoryID == item.CategoryID);
                int itemCost;

                if (pay != default)
                {
                    cost += (itemCost = pay.Price * days);
                }
                else
                {
                    itemCost = 0;
                }

                var itemCostString = itemCost == 0 ? "бесплатно" : $"{itemCost} р.";

                sb.AppendLine($"{item.FIO}, {item.Document}, {item.CategoryName} – {itemCostString}");
            }
            Ticket.Cost = cost;

            sb.AppendLine("════════════════");
            sb.AppendLine($"Итого: {cost} р.");

            SendMessage(sb.ToString());

            SendMessage("Оформляем?");
        }

        public class PechalkaAction : BaseAction
        {
            protected override void OnDo()
            {
                SendMessage($"Очень жаль, что Вы решили отказаться.{Environment.NewLine}Ждем Вас в снова!");
                userState.Clear();
            }
        }
    }
}