﻿using System;

namespace Sepulka.Sema.Metadata
{
    [Trigger("BlockedPeriod", "BlockedPeriod", "SelectStartDate")]
    [Trigger("BlockedDateBegin", "BlockedDateBegin", "SelectStartDate")]
    [Trigger("ValidPeriod", "PeriodSelected", "PeriodSelected")]
    [Trigger("ValidDateBegin", "DateBeginSelected", "DateBeginSelected")]
    [Trigger("Any", "InvalidPeriodOrDate", "SelectStartDate")]
    public class SelectStartDateState : BaseState
    {
    }

    public abstract class BaseDateRule : BaseRule
    {
        /// <summary>
        /// невалидная дата с причиной закрытия маршрута
        /// </summary>
        public static readonly DateTime InvalidMockDate = new DateTime(2019, 10, 10);

        /// <summary>
        /// проверить, что дата валидна
        /// </summary>
        /// <param name="date"> дата </param>
        /// <returns></returns>
        protected bool IsValidDate(DateTime date)
        {
            return date >= DateTime.Today;
        }

        /// <summary>
        /// проверить, что интервал валиден
        /// </summary>
        /// <param name="interval"> интервал </param>
        /// <returns></returns>
        protected bool IsValidInterval(Interval interval)
        {
            return IsValidDate(interval.Begin) &&
                   IsValidDate(interval.End) &&
                   interval.End >= interval.Begin;
        }
    }

    /// <summary>
    /// заблокированный период
    /// </summary>
    public class BlockedPeriod : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseInterval(text, out var value) &&
                IsValidInterval(value))
            {
                if (InvalidMockDate >= value.Begin.Date && InvalidMockDate <= value.End.Date)
                {
                    // дата заблокирована из-за метеоусловий
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// заблокированная дата начала
    /// </summary>
    public class BlockedDateBegin : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseDate(text, out var value) &&
                IsValidDate(value))
            {
                if (value.Date == InvalidMockDate)
                {
                    // дата заблокирована из-за метеоусловий
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// валидный период
    /// </summary>
    public class ValidPeriod : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseInterval(text, out var value) &&
                IsValidInterval(value))
            {
                Ticket.DateBegin = value.Begin;
                Ticket.DateEnd = value.End;
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// валидная дата начала
    /// </summary>
    public class ValidDateBegin : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseDate(text, out var value) &&
               IsValidDate(value))
            {
                Ticket.DateBegin = value;
                return true;
            }
            return false;
        }
    }

    public class BlockedPeriodAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"В выбранном вами периоде поход невозможен из-за сложных метеорологических условий.");
        }
    }

    public class BlockedDateBeginAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"В выбранную вами дату поход невозможен из-за сложных метеорологических условий.");
        }
    }

    public class InvalidPeriodOrDateAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Указан невалидный период или дата.{Environment.NewLine}Пожалуйста, введите снова.");
        }
    }

    [Trigger("BlockedPeriod", "BlockedPeriod", "DateBeginSelected")]
    [Trigger("BlockedDateEnd", "BlockedDateEnd", "DateBeginSelected")]
    [Trigger("ValidPeriod", "PeriodSelected", "PeriodSelected")]
    [Trigger("ValidDateEnd", "PeriodSelected", "PeriodSelected")]
    [Trigger("Any", "InvalidPeriodOrDate", "DateBeginSelected")]
    public class DateBeginSelectedState : BaseState
    {
    }

    public class DateBeginSelectedAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Когда планируете закончить поход?");
        }
    }

    public class BlockedDateEndAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"В выбранном вами периоде поход невозможен из-за сложных метеорологических условий.");
        }
    }

    /// <summary>
    /// заблокированная дата окончания/период
    /// </summary>
    public class BlockedDateEnd : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseDate(text, out var value) &&
                IsValidDate(value))
            {
                if (InvalidMockDate >= Ticket.DateBegin.Date && InvalidMockDate <= value.Date)
                {
                    // дата заблокирована из-за метеоусловий
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// валидная дата окончания
    /// </summary>
    public class ValidDateEnd : BaseDateRule
    {
        protected override bool OnCheck()
        {
            if (DateHelper.TryParseDate(text, out var value) &&
               IsValidDate(value) &&
               value >= Ticket.DateBegin)
            {
                Ticket.DateEnd = value;
                return true;
            }
            return false;
        }
    }
}
