﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Genesis.Seraphim;

namespace Sepulka.Sema.Metadata
{
    [Trigger("Yes", "ReactionsOrPhotos", "ReactionsOrPhotos")]
    [Trigger("No", "SelectStartDate", "SelectStartDate")]
    [Trigger("Reactions", "ShowReactions", "ShowReactions")]
    [Trigger("Photos", "ShowPhotos", "ShowReactions")]
    public class ServiceSelectedState : BaseState
    {
    }
    /// <summary>
    /// отзывы или фотографии
    /// </summary>
    [Trigger("Reactions", "ShowReactions", "ShowReactions")]
    [Trigger("Photos", "ShowPhotos", "ShowReactions")]
    [Trigger("Any", "ReactionsOrPhotos", "ReactionsOrPhotos")]
    public class ReactionsOrPhotosState : BaseState
    {
    }

    public class ReactionsRule : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "отзывы",
            "отзыв",
        };

        protected override bool OnCheck()
        {
            return SmartText.SimilarToVariants(_variants, text) > 0.8;
        }
    }

    public class PhotosRule : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "изображения",
            "картинки",
            "фотографии",
            "фоточки",
            "фотки",
            "фото",
        };

        protected override bool OnCheck()
        {
            return SmartText.SimilarToVariants(_variants, text) > 0.8;
        }
    }

    public class ReactionsOrPhotosAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage("Отзывы или фотографии?");
        }
    }

    public abstract class BaseReactionsOrPhotosAction : BaseAction
    {
        /// <summary>
        /// показать отзывы
        /// </summary>
        protected void ShowReactions()
        {
            // костыль, но что поделать :(
            // коллега, наполняющий базу, уснул
            var rnd = new Random(1000);
            var users = new string[] { "Иван", "Егор", "Петр", "Василий", "Геннадий", "Сёма", "Вован", "Евгений" };

            var service = bot.Data.Services.Find(e => e.ID == Ticket.Service.ID);
            if (service != default)
            {
                // ищем отзывы
                var reactions = bot.Data.Reactions.Where(e => e.ServiceID == service.ID).ToList();
                if (reactions.Count > 0)
                {
                    // отзывы есть
                    var sb = new StringBuilder();
                    sb.AppendLine("Вот что говорят об этом месте:");
                    sb.AppendLine();
                    foreach (var item in reactions.Take(3))
                    {
                        sb.AppendLine($"{users[rnd.Next(1, users.Length)]} пишет:");
                        sb.AppendLine($"{item.Text}");
                        sb.AppendLine($"Рейтинг: {new string('★', item.Rating)}");
                        sb.AppendLine();
                        sb.AppendLine("════════════════");
                        sb.AppendLine();
                    }

                    sb.Append("Ну что, оформляем билет?");

                    SendMessage(sb.ToString());
                    return;
                }
            }

            SendMessage($"Еще никто не оставлил отзывы о данном маршруте, вы можете стать первым ;-){Environment.NewLine}Ну что, оформляем билет?");
        }
    }

    public class ShowPhotosAction : BaseReactionsOrPhotosAction
    {
        protected override void OnDo()
        {
            var service = bot.Data.Services.Find(e => e.ID == Ticket.Service.ID);
            if (service != default && service.FileID > 0)
            {
                // файл найден

                // скачиваем файл
                var filename = bot.File.GetFileName(service.FileID);

                // отправляем фотографию
                SendImage(filename, ImageFormats.Jpg);
                Task.Delay(1000).ContinueWith(t =>
                {
                    ShowReactions();
                });
            }
            else
            {
                SendMessage("К величайшему сожалению, для данного маршрута не нашлось фотографий :(");
                Task.Delay(1000).ContinueWith(t =>
                {
                    ShowReactions();
                });
            }
        }
    }

    public class ShowReactionsAction : BaseReactionsOrPhotosAction
    {
        protected override void OnDo()
        {
            ShowReactions();
        }
    }

    [Trigger("Yes", "SelectStartDate", "SelectStartDate")]
    [Trigger("No", "HelloFirst", "Ready")]
    public class ShowReactionsState : BaseState
    {
    }
    
    public class SelectStartDateAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage("Какого числа хотите начать поход?");
        }
    }
}
