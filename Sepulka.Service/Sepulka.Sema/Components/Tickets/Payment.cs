﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using Genesis.Seraphim;
using Sepulka.Services;

namespace Sepulka.Sema.Metadata
{
    public class PaymentAction : BaseAction
    {
        protected override void OnDo()
        {
            if (Ticket.IDs == default)
            {
                ////////////////////////////////////////////////////////////////
                // сохраняем билеты
                ////////////////////////////////////////////////////////////////

                var route = Ticket.Service.Name;
                var dateBegin = Ticket.DateBegin;
                var dateEnd = Ticket.DateEnd;

                var ids = new List<int>();

                foreach (var item in Ticket.Participants)
                {
                    // формируем билет
                    var ticket = new Ticket
                    {
                        Route = route,
                        Document = item.Document,
                        Category = item.CategoryName,
                        FIO = item.FIO,
                        DateBegin = dateBegin,
                        DateEnd = dateEnd,
                    };

                    // сохраняем билет
                    var dbTicket = bot.Entity.PutEntityItem("Ticket", JObject.FromObject(ticket));
                    ids.Add(dbTicket["id"].Value<int>());
                }

                Ticket.IDs = ids;
            }

            var idsString = Ticket.IDs == default ? "1" : string.Join(",", Ticket.IDs.Select(e => e.ToString()).ToArray());
            var url = $"https://zapovednik.genesis.ru/payment?ticket_id={idsString}";
            SendMessage($"Вам выставлен счет на {Ticket.Cost} р.{Environment.NewLine}Просим перейти по ссылке для оплаты:{Environment.NewLine}{url}");

            // эмуляция оплаты
            Task.Delay(3000).ContinueWith(t =>
            {
                // оплата
                SendMessage($"Платеж прошел успешно.{Environment.NewLine}Ваши билеты ниже.");

                // формирование билетов
                var maker = new TicketMaker(Program.Configuration);

                foreach (var id in Ticket.IDs)
                {
                    var ticket = bot.Entity.GetEntityItem("Ticket", id).ToObject<Ticket>();
                    var image = maker.MakeTicket(ticket);

                    SendMessage($"Билет для {ticket.FIO}");
                    SendImage(image, ImageFormats.Png);
                }

                userState.Clear();
                bot.SaveUserState(userState);
            });
        }
    }
}