﻿using System;
using System.Linq;

namespace Sepulka.Sema.Metadata
{
    [Trigger("Yes", "WaitForService", "WaitForService")]
    [Trigger("No", "ServiceConsulting", "ServiceConsulting")]
    [Trigger("Help", "ServiceConsulting", "ServiceConsulting")]
    [Trigger("FullServiceName", "ServiceSelected", "ServiceSelected")]
    [Trigger("PartialServiceName", "ServiceAgree", "ServiceAgree")]
    [Trigger("AnySave", "ShowTopServicesActionAfterBadInput", "ShowTopServices")]
    public class ReservationSelectedState : BaseState
    {
    }

    [Trigger("FullServiceName", "ServiceSelected", "ServiceSelected")]
    [Trigger("PartialServiceName", "ServiceAgree", "ServiceAgree")]
    [Trigger("AnySave", "ShowTopServicesActionAfterBadInput", "ShowTopServices")]
    public class WaitForServiceState : BaseState
    {
    }

    public class WaitForServiceAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage(@"По какому маршруту ходите сходить?");
        }
    }

    public class ServiceSelectedAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Вы выбрали «{Ticket.Service.Name}».");
            SendMessage("Хотите почитать отзывы о маршруте или посмотреть фотографии??");
        }
    }

    /// <summary>
    /// полное совпадение
    /// </summary>
    public class FullServiceName : BaseRule
    {
        protected override bool OnCheck()
        {
            SaveLastInput();
            var result = SmartText.CheckFullDictionaryName(bot.Data.Services.Where(e => e.ReservationID == Ticket.Reservation.ID), text);
            if (result.Found)
            {
                // сохраняем маршрут
                userState.TicketData.Service = new DictionaryItem(result.Item);
            }
            return result.Found;
        }
    }

    /// <summary>
    /// частичное совпадение
    /// </summary>
    public class PartialServiceName : BaseRule
    {
        protected override bool OnCheck()
        {
            SaveLastInput();
            var result = SmartText.CheckPartialDictionaryName(bot.Data.Services.Where(e => e.ReservationID == Ticket.Reservation.ID), text);
            if (result.Found)
            {
                // сохраняем маршрут
                userState.TicketData.Service = new DictionaryItem(result.Item);
            }
            return result.Found;
        }
    }

    public class ServiceAgreeAction : BaseAction
    {
        protected override void OnDo()
        {
            SendMessage($"Вы имели в виду «{Ticket.Service.Name}»?");
        }
    }

    [Trigger("Yes", "ServiceSelected", "ServiceSelected")]
    [Trigger("No", "ShowTopServices", "ShowTopServices")]
    public class ServiceAgreeState : BaseState
    {
    }

    /// <summary>
    /// показать топ похожих
    /// </summary>
    public class ShowTopServicesAction : BaseAction
    {
        protected override void OnDo()
        {
            var top = SmartText.SelectTopByName(bot.Data.Services.Where(e => e.ReservationID == Ticket.Reservation.ID), userState.LastInput);
            var message = $"Предлагаю вам выбрать один из вариантов:{Environment.NewLine}{SaveAndFormatList(top)}";
            SendMessage(message);
        }
    }

    public class ShowTopServicesActionAfterBadInput : BaseAction
    {
        protected override void OnDo()
        {
            var top = SmartText.SelectTopByName(bot.Data.Services.Where(e => e.ReservationID == Ticket.Reservation.ID), userState.LastInput, SmartTextModes.ByWord);
            var message = $"К сожалению, я не уверен, что правильно вас понял.{Environment.NewLine}Вот что я могу предложить:{Environment.NewLine}{SaveAndFormatList(top)}";
            SendMessage(message);
        }
    }

    [Trigger("SelectVariant", "SelectTopService", "ServiceSelected")]
    [Trigger("Any", "Empty", "ShowTopServices")]
    public class ShowTopServicesState : BaseState
    {
    }

    public class SelectTopServiceAction : BaseAction
    {
        protected override void OnDo()
        {
            // сохраняем маршрут
            Ticket.Service = GetLastVariantDictionaryItem();
            SendMessage($"Вы выбрали «{Ticket.Service.Name}».");
            SendMessage("Хотите почитать отзывы о маршруте или посмотреть фотографии??");
        }
    }
}
