﻿namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// любая фраза с сохранением
    /// </summary>
    public class AnySave : BaseRule
    {
        protected override bool OnCheck()
        {
            SaveLastInput();
            return true;
        }
    }
}
