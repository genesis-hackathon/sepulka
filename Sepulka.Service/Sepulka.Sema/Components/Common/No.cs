﻿using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// нет
    /// </summary>
    public class No : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "нет",
            "не",
            "н",
            "не верно",
            "неверно",
            "-",

            // english
            "no",
            "n",
            "ytn",
            "yt",
        };

        public override bool Check(SemaBot bot, UserState state, string text)
        {
            return _variants.Contains(text.ToLowerInvariant());
        }
    }
}
