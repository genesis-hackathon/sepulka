﻿using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    public class Help : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "помочь",
            "помоги",
            "помогите",

            // english
            "help",
        };

        public override bool Check(SemaBot bot, UserState state, string text)
        {
            return _variants.Contains(text.ToLowerInvariant());
        }
    }
}
