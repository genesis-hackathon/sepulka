﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// выбор варианта из списка
    /// </summary>
    public class SelectVariant : BaseRule
    {
        private static readonly Regex _regVariant = new Regex(@"^(\d)[\s\.]", RegexOptions.Compiled);

        protected override bool OnCheck()
        {
            var variants = userState.LastVariants;
            if (variants != default)
            {
                if (!int.TryParse(text, out var value))
                {
                    // попытаемся регуляркой
                    var m = _regVariant.Match(text);
                    if (m.Success)
                    {
                        value = int.Parse(m.Groups[1].Value);
                    }
                    else
                    {
                        return false;
                    }
                }

                if (variants.Any(e => e.Option == value))
                {
                    userState.SelectedVariant = value;
                    return true;
                }
            }

            return false;
        }
    }
}