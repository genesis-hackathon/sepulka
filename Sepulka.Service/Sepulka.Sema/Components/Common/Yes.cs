﻿using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// да
    /// </summary>
    public class Yes : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "да",
            "д",
            "согласен",
            "конечно",
            "хорошо",
            "ок",
            "+",

            // english
            "ok",
            "yes",
            "y",
            "lf",
            "jr",
        };

        public override bool Check(SemaBot bot, UserState state, string text)
        {
            return _variants.Contains(text.ToLowerInvariant());
        }
    }
}
