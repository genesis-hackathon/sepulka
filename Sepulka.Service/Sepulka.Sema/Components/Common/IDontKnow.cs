﻿using System.Collections.Generic;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// не знаю
    /// </summary>
    public class IDontKnow : BaseRule
    {
        private readonly HashSet<string> _variants = new HashSet<string>
        {
            "не знаю",
            "не уверен",

            // english
            "i don't know",
            "i don't sure",
            "i dont know",
            "i dont sure",
        };

        public override bool Check(SemaBot bot, UserState state, string text)
        {
            return _variants.Contains(text.ToLowerInvariant());
        }
    }
}
