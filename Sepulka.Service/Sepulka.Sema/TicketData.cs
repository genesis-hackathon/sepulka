﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Sepulka.Sema
{
    /// <summary>
    /// данные билета
    /// </summary>
    public class TicketData
    {
        /// <summary>
        /// идентификаторы заказов
        /// </summary>
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<int> IDs { get; set; }

        /// <summary>
        /// заповедник
        /// </summary>
        [JsonProperty("reservation", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DictionaryItem Reservation { get; set; }

        /// <summary>
        /// маршрут
        /// </summary>
        [JsonProperty("service", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DictionaryItem Service { get; set; }

        /// <summary>
        /// начальная дата
        /// </summary>
        [JsonProperty("begin", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime DateBegin { get; set; }

        /// <summary>
        /// конечная дата
        /// </summary>
        [JsonProperty("end", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// участники
        /// </summary>
        [JsonProperty("participants", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Participant> Participants { get; set; }

        /// <summary>
        /// стоимость билета
        /// </summary>
        [JsonProperty("cost", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Cost { get; set; }

        /// <summary>
        /// признак однодневки
        /// </summary>
        [JsonIgnore]
        public bool IsOneDay => DateBegin == DateEnd;

        /// <summary>
        /// количество дней
        /// </summary>
        [JsonIgnore]
        public int DayCount => (int)Math.Floor((DateEnd - DateBegin).TotalDays) + 1;

        public void AddParticipant(Participant participant)
        {
            if (participant == default) return;
            if (Participants == default)
            {
                Participants = new List<Participant> { participant };
            }
            else
            {
                var origin = Participants.Find(e => e.Document == participant.Document);
                if (origin == default)
                {
                    // новый элемент
                    Participants.Add(participant);
                }
                else
                {
                    // существующий элемент
                    participant.CopyTo(origin);
                }
            }
        }
    }
}
