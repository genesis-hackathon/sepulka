﻿using System;

namespace Sepulka.Sema
{
    /// <summary>
    /// интервал времени
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{Begin.ToString(\"dd.MM\"),nq} - {End.ToString(\"dd.MM\"),nq}")]
    public class Interval
    {
        /// <summary>
        /// начальное время
        /// </summary>
        public DateTime Begin { get; set; }

        /// <summary>
        /// конечное время
        /// </summary>
        public DateTime End { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public Interval() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="begin"> начальное время </param>
        /// <param name="end"> конечное время </param>
        public Interval(DateTime begin, DateTime end) => (Begin, End) = (begin, end);

        /// <summary>
        /// пустой интервал
        /// </summary>
        public static Interval Empty { get; private set; } = new Interval();
    }
}
