﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sepulka.Sema
{
    /// <summary>
    /// реализация расстояния Левенштейна для строк
    /// </summary>
    public static class StringLevenshtein
    {
        /// <summary>
        /// регулярка для разбиения строки на токены
        /// </summary>
        private static readonly Regex _regSplit = new Regex(@"\s+", RegexOptions.Compiled);

        /// <summary>
        /// определить степень похожести строк (расстояние Левенштейна)
        /// </summary>
        /// <param name="s1"> строка 1 </param>
        /// <param name="s2"> строка 2 </param>
        /// <param name="relative"> указывает, что необходимо считать относительную похожесть </param>
        /// <returns></returns>
        public static double CompareStrings(string s1, string s2, bool relative = true)
        {
            s1 = s1.ToLowerInvariant();
            s2 = s2.ToLowerInvariant();

            int m = s1.Length, n = s2.Length;

            // вспомогательные переменные
            int i, j, a = 1, b = 0;
            char c1, c2;
            double[,] D = new double[2, n + 1];
            double costInsert = 1, costDelete = 1, costUpdate = 1;

            for (i = 0; i <= m; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    if (i != 0 || j != 0)
                    {
                        if (i == 0)
                        {
                            // считаем стоимость вставки
                            D[1, j] = D[1, j - 1] + costInsert;
                        }
                        else if (j == 0)
                        {
                            // считаем стоимость удаления
                            D[a, 0] = D[b, 0] + costDelete;
                        }
                        else
                        {
                            c1 = s1[i - 1];
                            c2 = s2[j - 1];
                            if (c1 != c2)
                            {
                                D[a, j] = Math.Min(Math.Min(
                                    D[b, j] + costDelete,
                                    D[a, j - 1] + costInsert),
                                    D[b, j - 1] + costUpdate
                                );
                            }
                            else
                            {
                                D[a, j] = D[b, j - 1];
                            }
                        }
                    }
                }
                (a, b) = (b, a);
            }

            return relative ? 1 - D[b, n] / n : n - D[b, n];
        }

        /// <summary>
        /// определить степень похожести строк (расстояние Левенштейна)
        /// </summary>
        /// <param name="s1"> строка 1 </param>
        /// <param name="s2"> строка 2 </param>
        /// <param name="D"> матрица </param>
        /// <param name="relative"> указывает, что необходимо считать относительную похожесть </param>
        /// <returns></returns>
        public static double CompareStrings(string s1, string s2, ref double[,] D, bool relative = true)
        {
            s1 = s1.ToLowerInvariant();
            s2 = s2.ToLowerInvariant();

            int m = s1.Length, n = s2.Length;

            // вспомогательные переменные
            int i, j, a = 1, b = 0;
            char c1, c2;
            if (D.GetLength(0) < 2 || D.GetLength(1) < n + 1) D = new double[2, n + 1];
            else
            {
                // очищаем массив
                for (j = 0; j <= n; j++) { D[0, j] = 0; D[1, j] = 0; }
            }
            double costInsert = 1, costDelete = 1, costUpdate = 1;

            for (i = 0; i <= m; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    if (i != 0 || j != 0)
                    {
                        if (i == 0)
                        {
                            // считаем стоимость вставки
                            D[1, j] = D[1, j - 1] + costInsert;
                        }
                        else if (j == 0)
                        {
                            // считаем стоимость удаления
                            D[a, 0] = D[b, 0] + costDelete;
                        }
                        else
                        {
                            c1 = s1[i - 1];
                            c2 = s2[j - 1];
                            if (c1 != c2)
                            {
                                D[a, j] = Math.Min(Math.Min(
                                    D[b, j] + costDelete,
                                    D[a, j - 1] + costInsert),
                                    D[b, j - 1] + costUpdate
                                );
                            }
                            else
                            {
                                D[a, j] = D[b, j - 1];
                            }
                        }
                    }
                }
                (a, b) = (b, a);
            }

            return relative ? 1 - D[b, n] / n : n - D[b, n];
        }

        /// <summary>
        /// определить степень похожести строк по словам (расстояние Левенштейна)
        /// </summary>
        /// <param name="s1"> строка 1 </param>
        /// <param name="tokens2"> фраза 2 </param>
        /// <param name="D"> матрица </param>
        /// <param name="relative"> указывает, что необходимо считать относительную похожесть </param>
        /// <returns></returns>
        public static double CompareMultiStrings(string s1, string[] tokens2, ref double[,] D, bool relative = true)
        {
            var tokens1 = Tokenize(s1);

            double best = 0, current;
            foreach (var e1 in tokens1)
            {
                foreach (var e2 in tokens2)
                {
                    if ((current = CompareStrings(e1, e2, ref D, relative)) > best)
                    {
                        best = current;
                    }
                }
            }

            return best;
        }

        /// <summary>
        /// разбить строку на токены
        /// </summary>
        /// <param name="text"> текст </param>
        /// <returns></returns>
        public static string[] Tokenize(string text)
        {
            var array = _regSplit.Split(text)
                .Where(e => e.Length >= 3)
                .ToArray();

            if (array.Length == 0)
            {
                return new string[] { text };
            }
            else
            {
                return array;
            }
        }
    }
}
