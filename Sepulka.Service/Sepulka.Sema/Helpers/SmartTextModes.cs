﻿namespace Sepulka.Sema
{
    /// <summary>
    /// режим сопоставления
    /// </summary>
    public enum SmartTextModes
    {
        /// <summary>
        /// сопоставление по фразе
        /// </summary>
        ByPhrase = 0,

        /// <summary>
        /// сопоставление по словам
        /// </summary>
        ByWord = 1,

        /// <summary>
        /// автоматический режим
        /// </summary>
        Auto = 2,
    }
}
