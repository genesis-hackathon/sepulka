﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sepulka.Sema
{
    /// <summary>
    /// функции для умного сопоставления строк
    /// </summary>
    public static class SmartText
    {
        private const int DEFAULT_D_SIZE = 100;

        /// <summary>
        /// степень похожести при полном совпадении
        /// </summary>
        public const double FULL_SIMILARITY = 0.95;

        /// <summary>
        /// степень похожести при частичном совпадении
        /// </summary>
        public const double PARTIAL_SIMILARITY = 0.75;

        /// <summary>
        /// количество выводимых элементов в топе
        /// </summary>
        public const int DEFAULT_TOP = 5;

        /// <summary>
        /// найти сущность с полным сопоставлением по фразе
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <returns></returns>
        public static SmartCheckResult<T> CheckFullDictionaryName<T>(IEnumerable<T> items, string text, SmartTextModes mode = SmartTextModes.ByPhrase)
            where T : IName
        {
            var result = FindBestByName(items, text, mode);
            if (result.Similarity < FULL_SIMILARITY) result.Found = false;
            return result;
        }

        /// <summary>
        /// найти сущность с частичным сопоставлением по фразе
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <param name="similarity"> степень похожести </param>
        /// <returns></returns>
        public static SmartCheckResult<T> CheckPartialDictionaryName<T>(IEnumerable<T> items, string text, SmartTextModes mode = SmartTextModes.ByPhrase, double similarity = PARTIAL_SIMILARITY)
            where T : IName
        {
            var result = FindBestByName(items, text, mode);
            if (result.Similarity < similarity) result.Found = false;
            return result;
        }

        /// <summary>
        /// найти лучшую сущность
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <returns></returns>
        public static SmartCheckResult<T> FindBestByName<T>(IEnumerable<T> items, string text, SmartTextModes mode)
            where T : IName
        {
            return FindBestByExpression(e => e.Name, items, text, mode);
        }

        /// <summary>
        /// найти лучшую сущность
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="func"> функция получения строки из сущности </param>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <returns></returns>
        public static SmartCheckResult<T> FindBestByExpression<T>(Func<T, string> func, IEnumerable<T> items, string text, SmartTextModes mode)
        {
            var D = new double[2, DEFAULT_D_SIZE];

            double best = 0, current;
            T bestItem = default;

            if (mode == SmartTextModes.Auto) mode = SelectAutoMode(text);

            if (mode == SmartTextModes.ByPhrase)
            {
                ////////////////////////////////////////////////////////////////
                // поиск по фразе
                ////////////////////////////////////////////////////////////////

                foreach (var item in items)
                {
                    var itemText = func(item);
                    if (itemText != default)
                    {
                        if ((current = StringLevenshtein.CompareStrings(text, itemText, ref D)) > best)
                        {
                            best = current;
                            bestItem = item;
                        }
                    }
                }
            }
            else
            {
                ////////////////////////////////////////////////////////////////
                // поиск по слову
                ////////////////////////////////////////////////////////////////

                throw new NotImplementedException();
            }

            return new SmartCheckResult<T>(bestItem != default, bestItem, best);
        }

        /// <summary>
        /// найти топ элементов по похожести
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <param name="count"> количество выводимых элементов </param>
        /// <returns></returns>
        public static List<T> SelectTopByName<T>(IEnumerable<T> items, string text, SmartTextModes mode = SmartTextModes.Auto, int count = DEFAULT_TOP)
            where T : IName
        {
            return SelectTop(e => e.Name, items, text, mode, count);
        }

        /// <summary>
        /// найти топ элементов по похожести
        /// </summary>
        /// <typeparam name="T"> тип сущности </typeparam>
        /// <param name="func"> функция получения строки из сущности </param>
        /// <param name="items"> элементы </param>
        /// <param name="text"> строка </param>
        /// <param name="mode"> режим сравнения </param>
        /// <param name="count"> количество выводимых элементов </param>
        /// <returns></returns>
        public static List<T> SelectTop<T>(Func<T, string> func, IEnumerable<T> items, string text, SmartTextModes mode = SmartTextModes.ByPhrase, int count = DEFAULT_TOP)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                // выводим просто топ элементов
                return items.Take(count).ToList();
            }
            else
            {
                var D = new double[2, DEFAULT_D_SIZE];

                if (mode == SmartTextModes.Auto) mode = SelectAutoMode(text);

                // сортируем по похожести и выводим
                if (mode == SmartTextModes.ByPhrase)
                {
                    ////////////////////////////////////////////////////////////////
                    // поиск по фразе
                    ////////////////////////////////////////////////////////////////

                    return items.Select(item => (similarity: StringLevenshtein.CompareStrings(text, func(item), ref D), item))
                        .OrderByDescending(e => e.similarity)
                        .Take(count)
                        .Select(e => e.item)
                        .ToList();
                }
                else
                {
                    ////////////////////////////////////////////////////////////////
                    // поиск по слову
                    ////////////////////////////////////////////////////////////////

                    var tokens = StringLevenshtein.Tokenize(text);

                    return items.Select(item => (similarity: StringLevenshtein.CompareMultiStrings(func(item), tokens, ref D), item))
                        .OrderByDescending(e => e.similarity)
                        .Take(count)
                        .Select(e => e.item)
                        .ToList();
                }
            }
        }

        /// <summary>
        /// проверить максимальную похожесть текста на один из вариантов
        /// </summary>
        /// <param name="variants"> список вариантов </param>
        /// <returns></returns>
        /// <param name="text"> текст </param>
        public static double SimilarToVariants(IEnumerable<string> variants, string text)
        {
            if (variants != default)
            {
                var D = new double[2, DEFAULT_D_SIZE];

                double best = 0, current;
                foreach (var item in variants)
                {
                    if ((current = StringLevenshtein.CompareStrings(item, text, ref D)) > best)
                    {
                        best = current;
                    }
                }
                return best;
            }
            return 0;
        }

        /// <summary>
        /// определить режим автоматически
        /// </summary>
        /// <param name="text"> фраза </param>
        /// <returns></returns>
        private static SmartTextModes SelectAutoMode(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) return SmartTextModes.ByPhrase;
            else if (text.Contains(' ')) return SmartTextModes.ByWord;
            else return SmartTextModes.ByPhrase;
        }
    }
}
