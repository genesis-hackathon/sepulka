﻿using System;

namespace Sepulka.Sema
{
    /// <summary>
    /// результат сопоставление
    /// </summary>
    /// <typeparam name="T"> тип сущности </typeparam>
    [System.Diagnostics.DebuggerDisplay("[{Found}] {Item,nq}")]
    public class SmartCheckResult<T>
    {
        /// <summary>
        /// признак нахождения элемента
        /// </summary>
        public bool Found { get; set; }

        /// <summary>
        /// найденный элемент
        /// </summary>
        public T Item { get; set; }

        /// <summary>
        /// степень похожести
        /// </summary>
        public double Similarity { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public SmartCheckResult() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="found"> признак нахождения элемента </param>
        public SmartCheckResult(bool found)
        {
            Found = found;
            Similarity = found ? 1 : 0;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="found"> признак нахождения элемента </param>
        /// <param name="item"> найденный элемент </param>
        public SmartCheckResult(bool found, T item)
        {
            Found = found;
            Item = item;
            Similarity = found ? 1 : 0;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="found"> признак нахождения элемента </param>
        /// <param name="item"> найденный элемент </param>
        /// <param name="similarity"> степень похожести </param>
        public SmartCheckResult(bool found, T item, double similarity)
        {
            Found = found;
            Item = item;
            Similarity = similarity;
        }
    }
}
