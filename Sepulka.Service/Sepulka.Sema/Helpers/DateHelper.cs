﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Sepulka.Sema
{
    /// <summary>
    /// помощник по обработке дат
    /// </summary>
    public static class DateHelper
    {
        private static readonly string[] formats = { "dd.MM", "dd.MM.yy", "dd.MM.yyyy", "d.MM", "d.MM.yy", "d.MM.yyyy", "MM/dd/yyyy", "yyyy-MM-dd" };
        private static readonly Regex _regInteral = new Regex(@"^([\d\.\/]+)\s*[-–]\s*([\d\.\/]+)$", RegexOptions.Compiled);

        private static readonly string[] _months = new string[] { "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря" };

        private static readonly Regex _regHuman = new Regex(@"^(\d+)\s+(\w+)$");

        /// <summary>
        /// попытаться прочитать дату
        /// </summary>
        /// <param name="str"> строка  </param>
        /// <param name="value"> значение </param>
        /// <returns></returns>
        public static bool TryParseDate(string str, out DateTime value)
        {
            if (DateTime.TryParseExact(str, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
            {
                return true;
            }

            var m = _regHuman.Match(str);
            if (m.Success)
            {
                if (!int.TryParse(m.Groups[1].Value, out var day)) return false;
                var month = GetMonth(m.Groups[2].Value);
                if (day < 32 && month >= 1 && month <= 12)
                {
                    try
                    {
                        value = new DateTime(DateTime.Now.Year, month, day);
                        return true;
                    }
                    catch { }
                }
            }

            return false;
        }

        /// <summary>
        /// попытаться прочитать интервал
        /// </summary>
        /// <param name="str"> строка  </param>
        /// <param name="value"> значение </param>
        /// <returns></returns>
        public static bool TryParseInterval(string str, out Interval value)
        {
            var m = _regInteral.Match(str);
            if (m.Success)
            {
                if (TryParseDate(m.Groups[1].Value, out var begin) &&
                    TryParseDate(m.Groups[2].Value, out var end))
                {
                    value = new Interval(begin, end);
                    return true;
                }
            }

            value = Interval.Empty;
            return false;
        }

        private static int GetMonth(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return 0;
            value = value.Trim().ToLowerInvariant();
            for (int i = 0; i < 12; i++)
            {
                if (_months[i] == value) return i + 1;
            }
            return 0;
        }
    }
}
