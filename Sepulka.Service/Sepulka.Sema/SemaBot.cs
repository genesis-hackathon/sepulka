﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.AspNetCore.Hosting.Internal;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Genesis.EPG.Services;
using Genesis.Seraphim;

using Sepulka.Sema.Data;

namespace Sepulka.Sema
{
    /// <summary>
    /// бот Сёма
    /// </summary>
    public class SemaBot : IDisposable
    {
        private const string USER_ENTITY = "User";

        private EntityService _entity;          // сервис сущностей
        private DictionaryService _dictionary;  // сервис справочников
        private FileService _file;              // файловый сервис

        private SeraphimConnector _connector;   // коннектор Серафима

        /// <summary>
        /// информация о пользователях
        /// </summary>
        private Dictionary<string, UserState> _users;

        /// <summary>
        /// сервис сущностей
        /// </summary>
        public EntityService Entity => _entity;

        /// <summary>
        /// файловый сервис
        /// </summary>
        public FileService File => _file;

        /// <summary>
        /// данные справочников
        /// </summary>
        public DictionaryData Data { get; set; }

        /// <summary>
        /// инициализировать сервис
        /// </summary>
        public void Init()
        {
            var config = Program.EPGConfig;

            _entity = new EntityService(config);
            _dictionary = new DictionaryService(config);

            // хардкод!!!
            _file = new FileService(config, new HostingEnvironment { ContentRootPath = @"C:\Work\Hackaton\Sepulka\Sepulka.Service\Sepulka.Service" });

            ////////////////////////////////////////////////////////////////
            // загружаем данные пользователей
            ////////////////////////////////////////////////////////////////

            _users = _entity.GetEntityData(USER_ENTITY).ToObjectArray<UserState>().ToDictionary(e => e.UserID);

            ////////////////////////////////////////////////////////////////
            // загружаем данные справочников
            ////////////////////////////////////////////////////////////////

            Data = new DictionaryData();
            Data.Load(_entity, _dictionary);
        }

        /// <summary>
        /// уничтожить объект
        /// </summary>
        public void Dispose()
        {
            if (_connector != default) _connector.Dispose();
        }

        /// <summary>
        /// запустить бота
        /// </summary>
        public void Run()
        {
            _connector = new SeraphimConnector();
            _connector.OnReveiveMessage += OnReveiveMessage;
            _connector.Connect();
            _connector.Begin();
        }

        /// <summary>
        /// получение сообщения
        /// </summary>
        private void OnReveiveMessage(object _, SeraphimEventArgs e)
        {
            // получем идентификатор отправителя
            var sender = e.Message.Sender;
            var text = e.Message.Text;

            if (sender != default && !string.IsNullOrWhiteSpace(text))
            {
                // получаем состояние пользователя
                var userState = GetUserState(sender);

                // получаем состояние бота
                var state = TypeContainer.GetState(userState.StateName ?? "None");
                if (state == default) state = TypeContainer.GetState("None");

                text = text.Trim();

                foreach (var item in state.Triggers)
                {
                    if (item.Rule.Check(this, userState, text))
                    {
                        // правило сработало

                        // выполняем действие
                        item.Action.Do(this, userState, item.Rule);

                        // меняем состояние
                        userState.StateName = item.NextState;
                        SaveUserState(userState);

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// получить состояние пользователя
        /// </summary>
        /// <param name="sender"> идентификатор отправителя </param>
        /// <returns></returns>
        private UserState GetUserState(string sender)
        {
            if (!_users.TryGetValue(sender, out var value))
            {
                value = new UserState
                {
                    UserID = sender,
                };

                // создаем связь с пользователем
                var user = _entity.PutEntityItem(USER_ENTITY, JObject.FromObject(value));
                value.ID = user["id"].Value<int>();

                _users.Add(value.UserID, value);
            }
            return value;
        }

        /// <summary>
        /// сохранить состояние пользователя
        /// </summary>
        /// <param name="userState"> состояние пользователя </param>
        public  void SaveUserState(UserState userState)
        {
            _entity.PostEntityItem("User", JObject.FromObject(userState), userState.ID);
        }

        /// <summary>
        /// отправить текстовое сообщение
        /// </summary>
        /// <param name="userState"> состояние пользователя </param>
        /// <param name="message"> текст сообщения </param>
        public void SendMessage(UserState userState, string message)
        {
            _connector.SendTextMessage(userState.UserID, message);
        }

        /// <summary>
        /// отправить графическое сообщение
        /// </summary>
        /// <param name="userState"> состояние пользователя </param>
        /// <param name="filename"> имя файла </param>
        /// <param name="imageFormat"> формат изображения </param>
        public void SendImage(UserState userState, string filename, string imageFormat)
        {
            var (image, thumbnail) = ImageHelper.GetImageAndThumbnail(filename);
            _connector.SendImageMessage(userState.UserID, image, thumbnail, imageFormat);
        }

        /// <summary>
        /// отправить графическое сообщение
        /// </summary>
        /// <param name="userState"> состояние пользователя </param>
        /// <param name="img"> изображение </param>
        /// <param name="imageFormat"> формат изображения </param>
        public void SendImage(UserState userState, Image img, string imageFormat)
        {
            var (image, thumbnail) = ImageHelper.GetImageAndThumbnail(img);
            _connector.SendImageMessage(userState.UserID, image, thumbnail, imageFormat);
        }
    }
}
