﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Sepulka.Sema.Data
{
    /// <summary>
    /// заповедник
    /// </summary>
    public class Reservation:DictionaryItem
    {
        /// <summary>
        /// банковский счет
        /// </summary>
        [JsonProperty("bill")]
        public string Bill { get; set; }
    }
}
