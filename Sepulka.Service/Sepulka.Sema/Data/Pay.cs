﻿using System;

using Newtonsoft.Json;

namespace Sepulka.Sema.Data
{
    /// <summary>
    /// оплата
    /// </summary>
    public class Pay : DictionaryItem
    {
        /// <summary>
        /// идентификатор маршрута
        /// </summary>
        [JsonProperty("service_id")]
        public int ServiceID { get; set; }

        /// <summary>
        /// идентификатор категории
        /// </summary>
        [JsonProperty("category_id")]
        public int CategoryID { get; set; }

        /// <summary>
        /// стоимость за день
        /// </summary>
        [JsonProperty("price")]
        public int Price { get; set; }
    }
}
