﻿using System;
using System.Text.RegularExpressions;

using Newtonsoft.Json;

namespace Sepulka.Sema.Data
{
    /// <summary>
    /// отзыв
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("[{ID}] {Text,nq}")]
    public class Reaction : DictionaryItem
    {
        /// <summary>
        /// описание
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }

        /// <summary>
        /// идентификатор маршрута
        /// </summary>
        [JsonProperty("service_id")]
        public int ServiceID { get; set; }

        /// <summary>
        /// рейтинг
        /// </summary>
        [JsonProperty("rating")]
        public int Rating { get; set; }

        /// <summary>
        /// идентификатор автора отзыва
        /// </summary>
        [JsonProperty("user_id")]
        public int UserID { get; set; }
    }
}
