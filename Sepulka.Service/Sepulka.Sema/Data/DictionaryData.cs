﻿using System;
using System.Collections.Generic;
using System.Linq;

using Genesis.EPG.Services;
using Genesis.Seraphim;

namespace Sepulka.Sema.Data
{
    /// <summary>
    /// данные справочников
    /// </summary>
    public class DictionaryData
    {
        /// <summary>
        /// заповедники
        /// </summary>
        public List<Reservation> Reservations { get; set; }

        /// <summary>
        /// маршруты
        /// </summary>
        public List<Service> Services { get; set; }

        /// <summary>
        /// отзывы
        /// </summary>
        public List<Reaction> Reactions { get; set; }

        /// <summary>
        /// информация об оплате
        /// </summary>
        public List<Pay> Payments { get; set; }

        /// <summary>
        /// заговнокодить чтение
        /// </summary>
        /// <param name="entityService"> сервис сущностей </param>
        /// <param name="dictionaryService"> сервис справочников </param>
        public void Load(EntityService entityService, DictionaryService dictionaryService)
        {
            Reservations = LoadDictionary<Reservation>(dictionaryService);
            Services = LoadDictionary<Service>(dictionaryService);
            Reactions = LoadEntity<Reaction>(entityService);
            Payments = LoadDictionary<Pay>(dictionaryService);
        }

        /// <summary>
        /// загрузить сущности
        /// </summary>
        /// <typeparam name="T"> тип </typeparam>
        /// <param name="service"> сервис </param>
        /// <param name="name"> имя </param>
        /// <returns></returns>
        private List<T> LoadEntity<T>(EntityService service, string name = default)
        {
            return service.GetEntityData(name ?? typeof(T).Name).ToObjectArray<T>();
        }

        /// <summary>
        /// загрузить справочник
        /// </summary>
        /// <typeparam name="T"> тип </typeparam>
        /// <param name="service"> сервис </param>
        /// <param name="name"> имя </param>
        /// <returns></returns>
        private List<T> LoadDictionary<T>(DictionaryService service, string name = default)
        {
            return service.GetDictionaryData(name ?? typeof(T).Name).ToObjectArray<T>();
        }
    }
}
