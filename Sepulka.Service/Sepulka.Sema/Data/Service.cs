﻿using System;
using System.Text.RegularExpressions;

using Newtonsoft.Json;

namespace Sepulka.Sema.Data
{
    /// <summary>
    /// маршрут
    /// </summary>
    public class Service : DictionaryItem
    {
        /// <summary>
        /// описание
        /// </summary>
        [JsonProperty("desc")]
        public string Description { get; set; }

        /// <summary>
        /// идентификатор заповедника
        /// </summary>
        [JsonProperty("reservation_id")]
        public int ReservationID { get; set; }

        /// <summary>
        /// продолжительность
        /// </summary>
        [JsonProperty("duration")]
        public string Duration { get; set; }

        /// <summary>
        /// идентификатор файла
        /// </summary>
        [JsonProperty("file_id")]
        public int FileID { get; set; }

        /// <summary>
        /// минимальная продолжительность маршрута
        /// </summary>
        [JsonIgnore]
        public int MinDuration => ParseDuration(Duration).min;

        /// <summary>
        /// максимальная продолжительность маршрута
        /// </summary>
        [JsonIgnore]
        public int MaxDuration => ParseDuration(Duration).max;

        /// <summary>
        /// средняя продолжительность маршрута
        /// </summary>
        [JsonIgnore]
        public double AverageDuration
        {
            get
            {
                var (min, max) = ParseDuration(Duration);
                return (min + max) * 0.5;
            }
        }

        private (int min, int max) ParseDuration(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                value = value.Trim();
                var m = Regex.Match(value, @"^(\d+)\s*-\s*(\d+)");
                if (m.Success)
                {
                    return (int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                }
                else
                {
                    m = Regex.Match(value, @"^(\d+)");
                    if (m.Success)
                    {
                        return (int.Parse(m.Groups[1].Value), int.Parse(m.Groups[1].Value));
                    }
                }
            }
            return (0, 1013);
        }
    }
}
