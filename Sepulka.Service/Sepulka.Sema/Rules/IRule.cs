﻿using System;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// правило
    /// </summary>
    public interface IRule
    {
        /// <summary>
        /// проверить действие правила
        /// </summary>
        /// <param name="bot"> бот </param>
        /// <param name="userState"> состояние </param>
        /// <param name="text"> текст сообщения </param>
        /// <returns></returns>
        bool Check(SemaBot bot, UserState userState, string text);
    }
}
