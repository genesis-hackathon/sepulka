﻿using System;

namespace Sepulka.Sema.Metadata
{
    /// <summary>
    /// базовое правило
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{GetType().Name,nq}")]
    public abstract class BaseRule : IRule
    {
        /// <summary>
        /// бот
        /// </summary>
        protected SemaBot bot;

        /// <summary>
        /// состояние пользователя
        /// </summary>
        protected UserState userState;

        /// <summary>
        /// текст сообщения
        /// </summary>
        protected string text;

        /// <summary>
        /// данные билета
        /// </summary>
        protected TicketData Ticket => userState.TicketData;

        /// <summary>
        /// проверить действие правила
        /// </summary>
        /// <param name="bot"> бот </param>
        /// <param name="userState"> состояние </param>
        /// <param name="text"> текст сообщения </param>
        /// <returns></returns>
        public virtual bool Check(SemaBot bot, UserState userState, string text)
        {
            this.bot = bot;
            this.userState = userState;
            this.text = text;

            return OnCheck();
        }

        /// <summary>
        /// проверить действие правила
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnCheck()
        {
            return false;
        }

        /// <summary>
        /// сохранить вввод пользователя
        /// </summary>
        /// <param name="text"> текст </param>
        protected void SaveLastInput(string text = default)
        {
            if (text == default) text = this.text;
            userState.LastInput = text;
        }
    }
}
