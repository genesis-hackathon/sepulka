﻿using System;

namespace Sepulka.Sema
{
    /// <summary>
    /// интерфейс сущности с идентификатором
    /// </summary>
    public interface IID
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        int ID { get; set; }
    }

    /// <summary>
    /// интерфейс сущности с наименованием
    /// </summary>
    public interface IName
    {
        /// <summary>
        /// наименование
        /// </summary>
        string Name { get; set; }
    }

    /// <summary>
    /// интерфейс справочника
    /// </summary>
    public interface IDictionaryItem : IID, IName
    {
    }
}
