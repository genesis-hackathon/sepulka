import Vue from 'vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU';
import 'element-ui/lib/theme-chalk/index.css';

import App from './views/App.vue';
import router from './router';
import store from './store';
import '@/styles/index.pcss';

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });

window._vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
