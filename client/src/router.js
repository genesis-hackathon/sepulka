import Vue from 'vue';
import Router from 'vue-router';
import ProxyView from '@/views/ProxyView.vue';
import PlacesList from '@/views/PlacesList.vue';
import PlacesItem from '@/views/PlacesItem.vue';
import PlacesBuy from '@/views/PlacesBuy.vue';
import Tickets from '@/views/Tickets.vue';
import TicketsItem from '@/views/TicketsItem.vue';
import Profile from '@/views/Profile.vue';
import Auth from '@/views/Auth.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/places'
        }, {
            path: '/places',
            component: ProxyView,
            children: [
                {
                    path: '/',
                    name: 'places',
                    component: PlacesList
                }, {
                    path: ':id',
                    component: ProxyView,
                    children: [
                        {
                            path: '/',
                            name: 'places-item',
                            component: PlacesItem
                        }, {
                            path: 'buy',
                            name: 'places-buy',
                            component: PlacesBuy
                        }
                    ]
                }
            ]
        }, {
            path: '/tickets',
            component: ProxyView,
            children: [
                {
                    path: '/',
                    name: 'tickets',
                    component: Tickets
                }, {
                    path: ':id',
                    name: 'tickets-item',
                    component: TicketsItem
                }
            ]
        }, {
            path: '/profile',
            name: 'profile',
            component: Profile
        }, {
            path: '/auth',
            name: 'auth',
            component: Auth
        }
    ]
});

router.beforeResolve((to, from, next) => {
    const session = window.localStorage.getItem('session');

    if (!session && to.name !== 'auth') {
        if (from.name === 'auth') return;
        else return router.replace('/auth');
    }
    next();
});

export default router;
