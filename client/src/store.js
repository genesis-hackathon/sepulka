import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { API } from '@/utils/constants';
import randomInt from '@/utils/randomInt';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null,
        routes: null,
        dictInit: false,
        dictMovement: null,
        dictPrice: null,
        dictComments: null,
        routeDetails: null,
        tickets: null,
        dictCommentators: [
            {
                id: 1,
                name: 'Алексей'
            }, {
                id: 2,
                name: 'Иван'
            }, {
                id: 3,
                name: 'Сергей'
            }, {
                id: 4,
                name: 'Анна'
            }, {
                id: 5,
                name: 'Валерий'
            }
        ]
    },

    mutations: {
        logIn(state, payload) {
            state.user = {
                login: payload.login,
                name: 'Василий',
                surname: 'Кравченко',
                patroname: 'Владимирович',
                fullName: 'Кравченко Василий Владимирович' ,
                serial: '5709',
                number: '467899'
            }
        },

        logOut(state) {
            state.user = null;
        },

        SET_DICTS(state, payload) {
            state.dictMovement = payload.movements;
            state.dictPrice = payload.prices;
            state.dictComments = payload.comments.map(e => ({
                ...e,
                // name: state.dictCommentators[Math.pow(e.user_id, 3) % 4].name
                name: state.dictCommentators[randomInt(0, 4)].name
            }));
            state.dictInit = true;
        },

        SET_ROUTES(state, payload) {
            state.routes = payload;
        },

        SET_ROUTE_DETAILS(state, payload) {
            const comments = state.dictComments.filter(k => k.service_id === payload.id);
            const sum = comments.reduce((acc, n) => acc + n.rating, 0);
            const rating = sum / comments.length;
            const price = state.dictPrice.find(c => c.service_id === payload.id && c.category_id === 3) || {};

            state.routeDetails = {
                ...payload,
                comments,
                rating: Number(rating.toFixed(1)),
                price: price.price,
                types: state.dictMovement.filter(m => payload.move_id.find(id => id === m.id))
            };
        },

        SET_TICKETS(state, payload) {
            state.tickets = payload;
        }
    },

    actions: {
        getDicts({ commit }) {
            return Promise.all([
                axios.get(API + '/Dictionary/Movement'),
                axios.get(API + '/Dictionary/Pay'),
                axios.get(API + '/Entity/Reaction')
            ])
                .then(([moves, prices, comments]) => {
                    commit('SET_DICTS', {
                        movements: moves.data,
                        prices: prices.data,
                        comments: comments.data
                    });
                })
                .catch(err => console.error(err));
        },

        async getRoutes({ state, commit, dispatch }) {
            if (!state.dictInit) await dispatch('getDicts');

            axios.get(API + '/Dictionary/Service')
                .then((res) => {
                    commit('SET_ROUTES', res.data.map(r => {
                        const types = state.dictMovement.filter(m => r.move_id.find(id => id === m.id));
                        const price = state.dictPrice.find(c => c.service_id === r.id && c.category_id === 3);
                        const comments = state.dictComments.filter(k => k.service_id === r.id);

                        const count = comments.length;
                        const ratingSum = comments.reduce((acc, n) => acc + n.rating, 0);
                        const rating = ratingSum / count;

                        return {
                            ...r,
                            types,
                            rating,
                            count,
                            price: price ? price.price : '-'
                        };
                    }));
                })
                .catch((err) => console.error(err));
        },

        async getRouteDetails({ state, commit, dispatch }, id) {
            if (!state.dictInit) await dispatch('getDicts');

            axios.get(API + '/Dictionary/Service/' + id)
                .then((res) => commit('SET_ROUTE_DETAILS', res.data))
                .catch((err) => console.error(err));
        },

        addTicket(context, ticket) {
            return axios
                .put(API + '/Entity/Ticket', ticket)
                .catch(err => console.error(err));
        },

        getTicket({ commit }, id) {
            axios.get(API + '/Entity/Ticket')
                .then(res => commit('SET_TICKETS', res.data))
                .catch(err => console.error(err));
        }
    },

    getters: {
        user: ({ user }) => user,
        routes: ({ routes }) => routes,
        routeDetails: ({ routeDetails }) => routeDetails,
        dictComments: ({ dictComments }) => dictComments,
        tickets: ({ tickets }) => tickets
    }
});
