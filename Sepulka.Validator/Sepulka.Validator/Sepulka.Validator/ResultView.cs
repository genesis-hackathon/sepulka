﻿using System;

namespace Sepulka.Validator
{
    /// <summary>
    /// представление результата
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{Status,nq}")]
    public class ResultView
    {
        /// <summary>
        /// статус
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// признак успеха
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public ResultView() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="status"> статус </param>
        /// <param name="isSuccess"> признак успеха </param>
        public ResultView(string status, bool isSuccess)
        {
            Status = status;
            IsSuccess = isSuccess;
        }
    }
}
