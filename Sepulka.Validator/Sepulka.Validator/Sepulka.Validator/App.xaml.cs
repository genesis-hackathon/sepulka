﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sepulka.Validator
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new ScanPage();
            //MainPage = new TicketPage(new Ticket
            //{
            //    Route = "Вишерский заповедник",
            //    FIO = "Грибков Роман Вадимович",
            //    Document = "1234 567890",
            //    DateBegin = DateTime.Today.AddDays(5),
            //    DateEnd = DateTime.Today.AddDays(8),
            //});
            //MainPage = new ResultPage(new ResultView($"Посетитель сошел с маршрута.{Environment.NewLine}Проводите его на правильный путь!", false));
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
