﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Newtonsoft.Json;

namespace Sepulka.Validator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanPage : ContentPage
    {
        // хардкодим публичный ключ
        private const string PRIVATE_KEY = "{\"D\":null,\"DP\":null,\"DQ\":null,\"Exponent\":\"AQAB\",\"InverseQ\":null,\"Modulus\":\"tB0xGooxp3OPozLokJ3ayhpX1IZBBuD8WqNoM6Ik4h1FbjHZlEMVwrd7mtYjWler\",\"P\":null,\"Q\":null}";

        public ScanPage()
        {
            InitializeComponent();

            scanner.OnScanResult += OnScanResult;
        }

        private void OnScanResult(ZXing.Result result)
        {
            var text = result.Text;
            if (string.IsNullOrWhiteSpace(text)) return;

            // проверка кода
            try
            {
                var parts = text.Split('|');
                if (parts.Length != 2) return;

                var ticketString = parts[0];
                var signBase64 = parts[1];

                // получаем билет
                var ticket = JsonConvert.DeserializeObject<Ticket>(ticketString);

                // проверяем подпись
                var data = Encoding.UTF8.GetBytes(ticketString);
                var sign = Convert.FromBase64String(signBase64);

                using (var rsa = new RSACryptoServiceProvider(384))
                {
                    rsa.ImportParameters(JsonConvert.DeserializeObject<RSAParameters>(PRIVATE_KEY));

                    if (!rsa.VerifyData(data, new SHA1CryptoServiceProvider(), sign))
                    {
                        // проверка не пройдена
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            scanner.IsVisible = false;
                            Application.Current.MainPage = new ResultPage(new ResultView("Предоставленный билет недействителен", false));
                        });
                    }
                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    scanner.IsVisible = false;
                    Application.Current.MainPage = new TicketPage(ticket);
                });
            }
            catch
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    scanner.IsVisible = false;
                    Application.Current.MainPage = new ResultPage(new ResultView($"Ошибка при распознавании билета", false));
                });
            }
        }
    }
}