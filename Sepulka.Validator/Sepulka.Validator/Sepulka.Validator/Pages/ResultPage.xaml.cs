﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sepulka.Validator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultPage : ContentPage
    {
        public ResultPage(ResultView view)
        {
            InitializeComponent();

            lblStatus.Text = view.Status;
            if (view.IsSuccess)
            {
                imgGood.IsVisible = true;
            }
            else
            {
                imgBad.IsVisible = true;
            }
        }

        private void Back_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ScanPage();
        }
    }
}