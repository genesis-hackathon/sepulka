﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sepulka.Validator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TicketPage : ContentPage
    {
        private readonly string STATUS_VALID = $"Посетитель может продолжить маршрут.{Environment.NewLine}Пожелайте ему доброго пути!";
        private readonly string STATUS_INVALID_PAST = $"Срок действия билета истек.{Environment.NewLine}Помогите посетителю вернуться!";
        private readonly string STATUS_INVALID_FUTURE = $"Срок действия не наступил.{Environment.NewLine}Помогите посетителю вернуться!";

        public TicketPage(Ticket ticket)
        {
            InitializeComponent();

            lblFIO.Text = ticket.FIO ?? "Не указано";
            lblDocument.Text = ticket.Document ?? "Не указан";
            lblRoute.Text = ticket.Route ?? "Не указан";
            lblTime.Text = ticket.IntervalDisplay;
            lblType.Text = ticket.Category ?? "Не указан";

            // проверяем валидность дат
            if (DateTime.Today >= ticket.DateBegin && DateTime.Today < ticket.DateEnd.AddDays(1))
            {
                // проверка пройдена
                lblStatus.Text = STATUS_VALID;
                imgGood.IsVisible = true;
            }
            else
            {
                // проверка провалена
                if (ticket.DateBegin > DateTime.Today)
                {
                    lblStatus.Text = STATUS_INVALID_FUTURE;
                }
                else
                {
                    lblStatus.Text = STATUS_INVALID_PAST;
                }
                imgBad.IsVisible = true;
            }
        }

        private void BtnSuccess_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ResultPage(new ResultView(STATUS_VALID, true));
        }

        private void BtnFailed_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ResultPage(new ResultView(STATUS_INVALID_PAST, false));
        }

        private void Back_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ScanPage();
        }
    }
}