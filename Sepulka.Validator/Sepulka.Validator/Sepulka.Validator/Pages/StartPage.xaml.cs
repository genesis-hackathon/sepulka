﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using ZXing.Mobile;

namespace Sepulka.Validator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class StartPage : ContentPage
    {
        /// <summary>
        /// конструктор
        /// </summary>
        public StartPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// перейти к распознаванию
        /// </summary>
        private void BtnScan_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ScanPage();
            //await Navigation.PushAsync(new NavigationPage(new ScanPage()));
        }
    }
}
